<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Hash;

class RandomHelper extends Controller
{
	public static function randomOTP($length = 6) 
	{
	    $pool = array_merge(range(0,9));
	    $key = "";
	    for($i=0; $i < $length; $i++) {
	        $key .= $pool[mt_rand(0, count($pool) - 1)];
	    }
	    return $key;
	}
	public static function randomKey($length = 8) 
	{
	    $pool = array_merge(range(0,9));
	    $key = "";
	    for($i=0; $i < $length; $i++) {
	        $key .= $pool[mt_rand(0, count($pool) - 1)];
	    }
	    return $key;
	}
}