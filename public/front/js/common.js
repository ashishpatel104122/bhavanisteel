// Header Sticky Start
const headerElement = document.getElementById("header-main");
const scrollUp = "scroll-up";
const scrollDown = "scroll-down";
var lastScroll = 0;

window.addEventListener("scroll", function() {
  if(headerElement){
    const currentScroll = window.pageYOffset;
    if (currentScroll == 0) {
      headerElement.classList.remove(scrollUp);
      return;
    }
    if (lastScroll < 0) {
      lastScroll = 0;
    }

    if (
      currentScroll > lastScroll &&
      !headerElement.classList.contains(scrollDown)
    ) {
      // down
      headerElement.classList.remove(scrollUp);
      headerElement.classList.add(scrollDown);
    } else if (
      currentScroll < lastScroll &&
      headerElement.classList.contains(scrollDown)
    ) {
      // up
      headerElement.classList.remove(scrollDown);
      headerElement.classList.add(scrollUp);
    }
    lastScroll = currentScroll;
  }
});
// Header Sticky End


// Mobile Side Navigation
$(document).ready(function () {
    // Open
    $("#hamburgerMenu").click(function () {
        $("#navBody").addClass("open");
        $('body').css('overflow', 'hidden');
    });
    // Close
    $("#mySidenav .icon-close").click(function () {
        $("#navBody").removeClass("open");
        $('body').css('overflow', 'scroll');
    });

    // Tabs
    $('.tabs li').click(function(){
      var tab_id = $(this).attr('data-tab');
      $('.tabs li').removeClass('active');
      $('.tab-content').removeClass('current');
      $(this).addClass('active');
      $("#"+tab_id).addClass('current');
    })
});

//  Accordion Start
var accHeading = document.querySelectorAll(".accordion");
var accPanel = document.querySelectorAll(".accordionPanel");

for (var i = 0; i < accHeading.length; i++) {
  // Execute whenever an accordion is clicked
  accHeading[i].onclick = function () {
    if (this.nextElementSibling.style.maxHeight) {
      hidePanels();
    } else {
      showPanel(this);
    }
  };
}

// Function to Show a Panel
function showPanel(elem) {
  hidePanels();
  elem.classList.add("active");
  elem.nextElementSibling.style.maxHeight =
    elem.nextElementSibling.scrollHeight + "px";
}

// Function to Hide all shown Panels
function hidePanels() {
  for (var i = 0; i < accPanel.length; i++) {
    accPanel[i].style.maxHeight = null;
    accHeading[i].classList.remove("active");
  }
}

// ===== FAQ Accordion Start
var faqHeading = document.querySelectorAll(".faqTitle");
var faqPanel = document.querySelectorAll(".faqContent");

for (var i = 0; i < faqHeading.length; i++) {
    faqHeading[i].onclick = function () {
        if (this.nextElementSibling.style.maxHeight) {
            hideFaqPanels();
        } else {
            showFaqPanel(this);
        }
    };
}

// Function to Show a Panel
function showFaqPanel(elem) {
    hideFaqPanels();
    elem.classList.add("active");
    elem.nextElementSibling.style.maxHeight = elem.nextElementSibling.scrollHeight + "px";
}

// Function to Hide all shown Panels
function hideFaqPanels() {
    for (var i = 0; i < faqPanel.length; i++) {
        faqPanel[i].style.maxHeight = null;
        faqHeading[i].classList.remove("active");
    }
}
// ===== FAQ Accordion End

// ========== Modal Popup Global Javascript Funciton Start
function modalToggle(id) {
  document.getElementById(id).classList.toggle("hide");
  document.getElementById(id).classList.toggle("modalHide");
  if(document.getElementById(id).classList.contains('modalHide')){
    document.body.style.overflow = "auto";
    }else{
    document.body.style.overflow = "hidden";
  }
}

window.addEventListener("load", function () {
  var modal = document.getElementsByClassName("openModal");
  for (var counter = 0; counter < modal.length; counter++) {
    modal[counter].addEventListener("click", function () {
      modalToggle(this.dataset.modalId);
    });
    {passive: true}
  }
});
{passive: true}
// ========== Modal Popup Global Javascript Funciton End