$(document).ready(function () {
    // Quick Product Slider
    $(".productListSlider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false
    });
    
    $('#listing-main .pro-card').click(function() {
        $(".productListSlider").slick('refresh');
    });
});

function openFilter() {
    var element = document.getElementById("filter-panel");
     element.classList.add("active");
   }
   
   function closeFilter() {
      var element = document.getElementById("filter-panel");
     element.classList.remove("active");
   }