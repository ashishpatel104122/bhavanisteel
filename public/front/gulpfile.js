const gulp = require('gulp');
const sass = require('gulp-sass');
const CleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
var minify = require('gulp-minify');

// Gulp Watch
gulp.task('watch', () => {
	gulp.watch('sass/**/*.scss', (done) => {
		console.log('watching');
		gulp.series([ /*'clean', */ 'sass'])(done);
	});
});

// Compile Scss
gulp.task('sass', () => {
	return gulp
		.src("sass/**/*.scss")
		.pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
		.pipe(gulp.dest("css/"));
});

// Build Bundle Css
gulp.task('bundle-css', function () {
	var sourceFiles = [
		'css/theme/**/*.css'
	];
	return gulp.src(sourceFiles)
		.pipe(concat('bundle.css'))
		.pipe(CleanCSS())
		.pipe(gulp.dest('public/build/css'));
});

// Bundle Js
gulp.task('bundle-js', function () {
	var sourceFiles = [
		'js/common.js',
		'js/slick.min.js',
		'js/home.js',
		'js/product.js'
	];
	return gulp.src(sourceFiles)
		.pipe(concat('bundle.js'))
		.pipe(minify())
		.pipe(gulp.dest('public/build/js'));
});

gulp.task('default', gulp.series(['bundle-css', 'sass', 'bundle-js']));