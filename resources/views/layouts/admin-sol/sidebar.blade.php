<!-- Left Sidebar - style you can find in sidebar.scss  -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav slimscrollsidebar">
		<div class="sidebar-head">
			<h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
		
		<ul class="nav" id="side-menu">
			<li class="sidebar-item @if(Request::route()->getName() == 'admin.dashboard') active @endif">
				<a href="{{ route('admin.dashboard') }}" class="sidebar-link has-arrow waves-effect has-error">
					<span class="hide-menu">Dashboard</span>
				</a>
		 	</li>
		 	
			
			<li class="sidebar-item @if(request()->segment(2) == 'categories' || Request::route()->getName() == 'admin.categories.search') active @endif">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
					<span class="hide-menu">Categories</span>
				</a>
				<ul aria-expanded="false" class="collapse first-level">
					<li class="sidebar-item">
						<a href="{{ route('admin.categories.index') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.categories.index' || Request::route()->getName() == 'admin.categories.search') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">List categories</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.categories.create') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.categories.create' || Request::route()->getName() == 'admin.categories.edit') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Create category</span>
						</a>
					</li>
				</ul>
		 	</li>
		 	
			<li class="sidebar-item @if(Request::route()->getName() == 'admin.products.index' || Request::route()->getName() == 'admin.products.create' || Request::route()->getName() == 'admin.products.edit' ) active  @endif">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
					<span class="hide-menu">Products</span>
				</a>
				<ul aria-expanded="false" class="collapse first-level">
					<li class="sidebar-item">
						<a href="{{ route('admin.products.index') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.products.index' || Request::route()->getName() == 'admin.products.search' ) active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Product Listing</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route('admin.products.create') }}" class="sidebar-link @if(Request::route()->getName() == 'admin.products.create' || Request::route()->getName() == 'admin.products.edit') active @endif">
							<i class="mdi mdi-adjust"></i>
							<span class="hide-menu">Add Product</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="sidebar-item @if(Request::route()->getName() == 'admin.cms.list') active @endif">
				<a href="{{ route('admin.cms.list') }}" class="sidebar-link has-arrow waves-effect has-error">
					<span class="hide-menu">CMS</span>
				</a>
		 	</li>
		 	<li class="sidebar-item @if(Request::route()->getName() == 'admin.contactus.list') active @endif">
				<a href="{{ route('admin.contactus.list') }}" class="sidebar-link has-arrow waves-effect has-error">
					<span class="hide-menu">Enquiry</span>
				</a>
		 	</li>
		</ul>
	</div>
</div>