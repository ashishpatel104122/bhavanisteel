<?php 
namespace App\Helpers;

use Illuminate\Database\Eloquent\Helper;
use Mail;
use App\Helpers\Exceptions;
use Log;

class MailHelper
{
	public static function contactUs($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.customer.customer_contactus',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Hawkio ContactUs');
			        $message->from('shineinfosoft23@gmail.com','Hawkio');
			    });
			    $adminMail = Mail::send('emails.admin.customer_contactus',['data'=>$data] , function($message) use ($data) {
			        $message->to('shineinfosoft23@gmail.com', 'Admin')->subject('Hawkio ContactUs');
			        $message->from('shineinfosoft23@gmail.com','Hawkio');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            // Exceptions::exception($e);
        }
	}
	public static function EmployeeCreate($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.admin.employer_create',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['first_name'])->subject('Hawkio Employer Create');
			        $message->from('shineinfosoft23@gmail.com','Hawkio');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            Log::debug($e);
        }
	}
	public static function EmployeePasswordChange($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.admin.employer_password_change',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['first_name'])->subject('Hawkio Employer Password Change');
			        $message->from('shineinfosoft23@gmail.com','Hawkio');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            Log::debug($e);
        }
	}
	public static function statusActive($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.admin.employer_status_Active',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['first_name'])->subject('Hawkio Employer Account Active');
			        $message->from('shineinfosoft23@gmail.com','Hawkio');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            Log::debug($e);
        }
	}
	public static function statusInActive($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.admin.employer_status_InActive',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['first_name'])->subject('Hawkio Employer Account InActive');
			        $message->from('shineinfosoft23@gmail.com','Hawkio');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            Log::debug($e);
        }
	}
	public static function sendpo($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.customer.sendpo',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Hawkio Po');
			        $message->from('shineinfosoft23@gmail.com','Hawkio');
			        $message->attach(public_path($data['po']));
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            Log::error($e);
        }
	}
	public static function checkin_checkout($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.customer.checkin_checkout',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Hawkio Po');
			        $message->from('shineinfosoft23@gmail.com','Hawkio');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            Log::error($e);
        }
	}

	public static function userIDMail($data="")
	{
		try {
			if($data=="") {
				return "false";
			} else{
				// dd($data);
				$sendMail = Mail::send('emails.customer.userDetail',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Hawkio Po');
			        $message->from('shineinfosoft23@gmail.com','Hawkio');
			    });
			    if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch(Exception $e) {
            Log::error($e);
        }
	}
	
}