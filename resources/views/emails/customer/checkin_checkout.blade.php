<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
  <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
    <div style="padding: 40px; background: #fff;">
      <table style="width: 100%;" cellspacing="0" cellpadding="0" border="0">
        <tbody>
          <tr>
            <td>  
              <b>Dear admin,</b>
              <p> Login details are here.</p>
              <p>Customer Name : <b>{{ $data['name'] }}</b></p>
              <p>Email id : <b>{{ $data['email'] }}</b></p>
              <p>Date : <b>{{ $data['date'] }}</b></p>
              <p>Address : <b>{{ $data['address'] }}</b></p>
              <p>Status : <b>{{ $data['status'] }}</b></p>
              <b>- Thanks (Hawkio team)</b>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

</body>
</html>
