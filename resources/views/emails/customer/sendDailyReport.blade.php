<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
  <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
    <div style="padding: 40px; background: #fff;">
      <table style="width: 100%;" cellspacing="0" cellpadding="0" border="0">
        <tbody>
          <tr>
            <td>  
              <b>Dear Admin</b>
              <p><b> Customer Details</b></p>
              <p>Name: <b>{{$data['customer']->name}}</b></p>
              <p>Email: <b>{{$data['customer']->email}}</b></p>
              <p>Contact No: <b>{{$data['customer']->phone}}</b></p>
              <p><b> Today's PO Details</b></p>
                @if(count($data['daily_reports'])!=0)
                <table class="table table-bordered mb-0" style="margin-bottom: 0!important;border: 1px solid #e3e6f0;width: 100%;color: black;border-collapse: collapse;display: table;border-color: grey; font-size: 12px;">
                  <thead>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">Name</th>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">City</th>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">Mobile</th>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">Time</th>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">PO</th>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">GST Image</th>
                  </thead>
                  <tbody>
                    @foreach($data['daily_reports'] as $daily_report)
                    
                      <tr>
                        <td style="text-align: center!important; border: 1px solid #111111 !important; color: black">{{ $daily_report->name }}</td>
                        <td style="text-align: center!important; border: 1px solid #111111 !important; color: black">{{ $daily_report->city }}</td>
                        <td style="text-align: center!important; border: 1px solid #111111 !important; color: black">{{ $daily_report->mobile }}</td>
                        <td style="text-align: center!important; border: 1px solid #111111 !important; color: black">{{ date('d-m-Y H:i:s',strtotime($daily_report->created_at)) }}</td>
                        <td style="text-align: center!important; border: 1px solid #111111 !important; color: black"><a href="{{ env('WEB_URL') }}{{$daily_report->po}}">Check PO</a></td>
                        @if($daily_report->gst_image)
                          <td style="text-align: center!important; border: 1px solid #111111 !important; color: black"><a href="{{ env('WEB_URL') }}{{$daily_report->gst_image}}">Check gst_image</a></td>
                        @else
                          <td></td>
                        @endif
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                @else
                  <p style="margin-left: 10px; margin-top: -10px;">Today's PO Data Not Available</p>
                @endif
              <p><b> Today's Visitor Dealers </b></p>
                @if(count($data['dealers'])!=0)
                <table class="table table-bordered mb-0" style="margin-bottom: 0!important;border: 1px solid #e3e6f0;width: 100%;color: black;border-collapse: collapse;display: table;border-color: grey; font-size: 12px;">
                  <thead>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">Name</th>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">City</th>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">Mobile</th>
                    <th style="text-align: center!important; border: 1px solid #111111 !important; font-size: 12px!important; color: black">Time</th>
                  </thead>
                  <tbody>
                    @foreach($data['dealers'] as $dealer)
                      <tr>
                        <td style="text-align: center!important; border: 1px solid #111111 !important; color: black">{{ $dealer->name }}</td>
                        <td style="text-align: center!important; border: 1px solid #111111 !important; color: black">{{ $dealer->city }}</td>
                        <td style="text-align: center!important; border: 1px solid #111111 !important; color: black">{{ $dealer->mobile }}</td>
                        <td style="text-align: center!important; border: 1px solid #111111 !important; color: black">{{ date('d-m-Y H:i:s',strtotime($dealer->created_at)) }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                @else
                  <p style="margin-left: 30px; margin-top: -10px;">Today's Visitor Dealer Data Not Available</p>
                @endif
              <b>- Thanks (Hawkio team)</b>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

</body>
</html>
