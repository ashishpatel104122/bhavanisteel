@extends('customers.layout.default')
@section('content')
<section>
     <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-breadcrumb">
                    <h2>Login & Register</h2>
                    <ul class="breadcrumb">
                        <li><a href="{{ route('customer.home') }}"><span class="fa fa-home"></span> Home</a></li>
                        @if(Auth::check())
                            <li class="active"></li>
                        @else
                            <li class="active">Login & Register</li>
                        @endif
                    </ul>
                </div>
                @if(Request::input('login')=="wish")
                <div class="alert alert-danger col-sm-12">You have not logged in.</div>
                @endif
                <div class="tab-login-signin">
                    <div class="row">
                    @if(Auth::check())
                        <div class="alert alert-success col-sm-12">You have already logged in.</div>
                    @else
                        <div class="col-sm-5">
                            <h2>Login</h2>
                            <div style="text-align: center;display:none;" class="alert alert-danger col-sm-12 email-credential"></div>
                            <form class="customer-signup" action="{{ route('customer.login') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Email address:<span class="red">*</span></label>
                                    <input type="email" name="email" class="form-control" maxlength="50">
                                     @if($errors->has('email'))
                                        <span class="text-danger">{{$errors->first('email')}}</span>
                                    @endif 
                                </div>
                                <div class="form-group">
                                    <label>Password:<span class="red">*</span></label>
                                    <input type="password" name="password" class="form-control" maxlength="50">
                                     @if($errors->has('password'))
                                        <span class="text-danger">{{$errors->first('password')}}</span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-default subscribe">Login</button>
                            </form>
                        </div>
                        <div class="col-sm-2">
                            <div class="or-div">
                                <span>OR</span>
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <h2>Signup</h2>
                            <form class="customer-signup" action="{{ route('customer.register') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Name <span class="red">*</span></label>
                                    <input type="text" name="name" onkeypress="return onlyAlphabets(event,this);" maxlength="100" class="form-control" placeholder="Name" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Organisation <span class="red">*</span></label>
                                    <input type="text" maxlength="100" name="organisation" class="form-control" placeholder="Organisation" value="{{old('organisation')}}">
                                    @if($errors->has('organisation'))
                                        <span class="text-danger">{{$errors->first('organisation')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Email Adderss <span class="red">*</span></label>
                                    <input type="email" name="email" maxlength="50" class="form-control" placeholder="Email" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <span class="text-danger">{{$errors->first('email')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Phone Number<span class="red">*</span> </label>
                                    <input type="text" onkeypress="return isNumber(event)" name="phone" maxlength="10" placeholder="Phone" class="form-control" value="{{old('phone')}}">
                                    @if($errors->has('phone'))
                                        <span class="text-danger">{{$errors->first('phone')}}</span>
                                    @endif
                                </div>
                                 <div class="form-group">
                                    <label>Password <span class="red">*</span></label>
                                    <input type="password" maxlength="50" placeholder="Password" name="password" class="form-control" >
                                    @if($errors->has('password'))
                                        <span class="text-danger">{{$errors->first('password')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password <span class="red">*</span></label>
                                    <input type="password" placeholder="Confirm Password" maxlength="50" name="passwordCon" class="form-control">
                                    @if($errors->has('passwordCon'))
                                        <span class="text-danger">{{$errors->first('passwordCon')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Profile<span class="red">*</span></label>
                                    <input type="file" name="image" accept="image/*" class="form-control">
                                    @if($errors->has('image'))
                                        <span class="text-danger">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Address<span class="red">*</span></label>
                                    <textarea name="address_line1" placeholder="Address" cols="55" rows="5" class="form-control"  maxlength="500">{{old('address_line1')}}</textarea>
                                    @if($errors->has('address_line1'))
                                        <span class="text-danger">{{$errors->first('address_line1')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Country<span class="red">*</span></label>
                                    <select name="country" readonly class="form-control">
                                        <option value="101">India</option>
                                    </select>
                                    @if($errors->has('country'))
                                        <span class="text-danger">{{$errors->first('country')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>State<span class="red">*</span></label>
                                    <select name="state" id="state" class="form-control" tabindex="4">
                                        <option value="">State</option>
                                        @foreach($states as $state)
                                        <option value="{{$state->state_id}}" @if(old('state')==$state->state_id)selected="true" @endif>{{$state->state}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="selected_state" id="selected_state" value="{{old('state')}}">
                                    @if($errors->has('state'))
                                        <span class="text-danger">{{$errors->first('state')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>City<span class="red">*</span></label>
                                    <select name="city" class="form-control" tabindex="5" id="city" value="{{old('city')}}">
                                        <option value="">Select city</option>
                                    </select>
                                    <input type="hidden" name="selected_city" id="selected_city" value="{{old('city')}}">
                                    @if($errors->has('city'))
                                        <span class="text-danger">{{$errors->first('city')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Post Code<span class="red">*</span></label>
                                    <input type="text" onkeypress="return isNumber(event)" maxlength="6" value="{{old('post_code')}}" placeholder="Post Code" name="post_code" class="form-control">
                                    @if($errors->has('post_code'))
                                        <span class="text-danger">{{$errors->first('post_code')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Pan Card<span class="red">*</span></label>
                                    <input type="text" maxlength="50" onkeypress="return isNumber(event)" value="{{old('pan_card')}}" placeholder="Pan Card" name="pan_card" class="form-control">
                                    @if($errors->has('pan_card'))
                                        <span class="text-danger">{{$errors->first('pan_card')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Aadhar Card<span class="red">*</span></label>
                                    <input type="text" maxlength="12" placeholder="Aadhar Card" onkeypress="return isNumber(event)" name="aadhar_card" value="{{old('aadhar_card')}}"  class="form-control">
                                    @if($errors->has('aadhar_card'))
                                        <span class="text-danger">{{$errors->first('aadhar_card')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Gst No<span class="red">*</span></label>
                                    <input type="text" maxlength="15" value="{{old('gst_no')}}" placeholder="Gst No" onkeypress="return isNumber(event)" name="gst_no" class="form-control">
                                    @if($errors->has('gst_no'))
                                        <span class="text-danger">{{$errors->first('gst_no')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Cst No<span class="red">*</span></label>
                                    <input type="text" name="cst_no" value="{{old('cst_no')}}" placeholder="Cst No" onkeypress="return isNumber(event)" maxlength="11" class="form-control">
                                    @if($errors->has('cst_no'))
                                        <span class="text-danger">{{$errors->first('cst_no')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Tin No<span class="red">*</span></label>
                                    <input type="text" name="tin_no" value="{{old('tin_no')}}" placeholder="Tin No" onkeypress="return isNumber(event)" maxlength="12" class="form-control">
                                    @if($errors->has('tin_no'))
                                        <span class="text-danger">{{$errors->first('tin_no')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea name="notes" placeholder="Notes" cols="55" rows="5" class="form-control"  maxlength="500">{{old('notes')}}</textarea>
                                    @if($errors->has('notes'))
                                        <span class="text-danger">{{$errors->first('notes')}}</span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-default subscribe">Signup</button>
                            </form>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
<style type="text/css">
    .red{
        color: red;
    }
    .fa{
        margin-top: 4px!important;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if($("#selected_state").val()!=""){
            state_id = $("#selected_state").val();
            getcity(state_id);
        }
        $("#state").on('change',function(){
            state_id = $("#state").val();
            getcity(state_id);
        })
        $('#from_date').datepicker({
            autoclose: true,  
            format: "dd/mm/yyyy",
            todayHighlight: true,

        })
        $('#to_date').datepicker({
            autoclose: true,  
            format: "dd/mm/yyyy",
            todayHighlight: true,

        })

    });
    function getcity(state_id){
        selected_city = $("#selected_city").val();
        if(state_id!=null){
            $.ajax({
                url:"{{ url('admin/getcity') }}/"+state_id,
                type:"GET",             
                dataType:"json",
                success:function(data) {
                    console.log(data);
                    $('#city').empty();
                    $('#city').append('<option value="">Select city</option>');
                    $.each(data, function(key, value){
                        if(selected_city==value){
                            $('#city').append('<option value="'+ value +'" selected="true">' + key + '</option>');
                        } else{
                            $('#city').append('<option value="'+ value +'">' + key + '</option>');
                        }
                    });
                },
                error: function(error){
                }
            });
        }
    }
</script>

