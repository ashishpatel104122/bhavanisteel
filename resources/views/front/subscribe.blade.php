<section class="subscribe-us">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 xs-p-0">
                <div class="subscribe-text">
                    <h3>Subscribe Us <span>For Latest Updates</span></h3>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="subscribe-now">
                    <form>
                        <div class="row">
                            <div class="col-sm-4 col-xs-6">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email *">
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="First Name *">
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="btn-part">
                                    <button type="submit" id="sub_btn" class="btn btn-default subscribe">Subscribe  NOW</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    	</div>
	</div>
</section>
<script type="text/javascript">
    document.getElementById("sub_btn").addEventListener("click", function(event){
        alert("Comming soon.");
        event.preventDefault()
    });
</script>