@forelse($products as $product)
    @php
        $prod_json = json_decode($product->json);
    @endphp
    <div class="col-sm-4 col-md-4 col-lg-3 col-xs-6 five-box">
        <div class="list-full">
            <div class="img-bdr">
                <img src="@if($product->cover!=null){{ $product->cover }}@else{{ asset("images/placeholder.png") }}@endif" alt="">
                <div class="control-group">
                    <input class="red-heart-checkbox" id="product_favourite_{{ $product->id }}" type="checkbox">
                    <label for="product_favourite_{{ $product->id }}"></label>
                </div>
            </div>
            <div class="detail-hover">
                <h3>{{ $product->name }}</h3>
                <span>{{ \Symfony\Component\Intl\Intl::getCurrencyBundle()->getCurrencySymbol($prod_json->Price->CurrencyCode).$product->sale_price }}</span>
                <div class="btn-part">
                    <a href="{{ route('customer.details', [$product->slug]) }}"><button type="submit" class="btn btn-default subscribe">VIEW MORE</button></a>
                </div>
            </div>
        </div>
    </div>
@empty
@endforelse
<input type="hidden" id="next_page_url" value="@if($products->nextPageUrl()!=''){{ $products->nextPageUrl() }}&sort={{ request()->input('sort') }}@endif">