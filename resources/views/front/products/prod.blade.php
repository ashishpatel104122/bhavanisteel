@extends('customers.layout.default')
@section('content')
<section class="slider-part inner-slider">
	<img src="{{ url('images/product-listing-inner.jpg') }}" alt="">
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="page-filter">
					
					<div class="form-group text-right">
                         {{-- <label>Sort by:</label>  --}}
                        <select class="form-control w-auto d_inline" name="sort" id="sort_change">
                            <option value="" @if(request()->input('sort')=="") selected="true" @endif>Sort by</option>
                            <option value="newest" @if(request()->input('sort')=="newest") selected="true" @endif>Newest</option>
                            <option value="low_to_high" @if(request()->input('sort')=="low_to_high") selected="true" @endif>Price: Low to High</option>
                            <option value="high_to_low" @if(request()->input('sort')=="high_to_low") selected="true" @endif>Price: High to Low</option>
                            <option value="name" @if(request()->input('sort')=="name") selected="true" @endif>Name</option>
                            <option value="bestseller" @if(request()->input('sort')=="bestseller") selected="true" @endif>Bestsellers</option>
                        </select>  
                    </div>

				</div>
			</div>
		</div>
	</div>
</section>


<section class="product-listing">
    <div class="container">
        <div class="row" id="product_listing">
        	@forelse($products as $product)
        		@php
        			$prod_json = json_decode($product->json);
        		@endphp
	            <div class="col-sm-4 col-md-4 col-lg-3 col-xs-6 five-box">
	                <div class="list-full">
	                    <div class="img-bdr">
	                        <img src="@if($product->cover!=null){{ $product->cover }}@else{{ asset("images/placeholder.png") }}@endif" alt="">
	                        <div class="control-group">
	                            <input class="red-heart-checkbox wishlist-heart" id="{{ $product->id }}" name="add_wish"type="checkbox">
	                          	<label for="{{ $product->id }}"></label>
	                        </div>
	                    </div>
	                    <div class="detail-hover">
	                        <h3>{{ $product->name }}</h3>
	                        <span>{{ \Symfony\Component\Intl\Intl::getCurrencyBundle()->getCurrencySymbol($prod_json->Price->CurrencyCode).$product->sale_price }}</span>

	                        <div class="btn-part">
	                        	<a href="{{ route('customer.details', [$product->slug]) }}"><button type="submit" class="btn btn-default subscribe">VIEW MORE</button></a>
	                        </div>
	                    </div>
	                </div>
	            </div>
			@empty
        	@endforelse
        	<input type="hidden" id="next_page_url" value="@if($products->nextPageUrl()!=''){{ $products->nextPageUrl() }}&sort={{ request()->input('sort') }}@endif">
		</div>
    	<div id="loading_div" style="display: none;">
    		<center><img src="{{ asset('images/loading.gif') }}"></center>
    	</div>
		<div class="row m-t-50">
			<div class="col-lg-12">
				<center>
					<input type="button" class="btn btn-default" id="load_more" value="Show More"/>
				</center>
			</div>
		</div>
	</div>
</section>

@include('front.subscribe')

@push('js')
<script type="text/javascript">
$(document).ready(function(){
	/*variable*/
	var next_page_url = null,page_url = null;
	$("#load_more").on('click',function(){
		$("#load_more").css('pointer-events','none');
		$("#loading_div").css('display','block');
		next_page_url = $("#next_page_url").val();
		if(next_page_url!=null && next_page_url.length != 0){
			$.ajax({
	            url:next_page_url,
	            type:"GET",
	            success:function(data) {
	            	$("#next_page_url").remove();
	            	$('#product_listing').append(data);
	            	page_url = $("#next_page_url").val();
	            	if(page_url.length == 0){
						$("#load_more").hide();
	            	}
	            	$("#loading_div").css('display','none');
	            	$("#load_more").css('pointer-events','auto');
	            },
	            error: function(error){
	            	$("#load_more").css('pointer-events','auto');
	            }
	        });
		}
	});

	$("#sort_change").on('change',function(){
		var sort = $("#sort_change").val();
		window.location.href = "{{ request()->url() }}?sort="+sort;
	});

	$(".wishlist-heart").on('click',function(){
		var product_id = $(this).attr('id');
		var check_login = "{{ Auth::check() }}";
		if ($(this).prop('checked')==true){
			/* heart check */
			if(check_login){
				/* login check */
				wishlistAjax(product_id,'check');
			} else{
				$('#LoginSignup').modal('show');
		        $(this).prop("checked", false);
			}
		} else{
			/* heart uncheck */
			wishlistAjax(product_id,'uncheck');
		}
	});
})

function wishlistAjax(id,type)
{
	$.ajax({
        url: "{{route('customer.wishdetails')}}",
        type: 'GET',
        data : { id:id,type:type },
		success: function(data){
            console.log(data);
        },error:function(error){
        	console.log(error);
        }
    });
}
</script>
@endpush

@endsection
