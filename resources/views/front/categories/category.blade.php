@extends('customers.layout.default')
@section('content')
    
    {{-- Slider Image --}}
    <section class="slider-part inner-slider">
        <img src="{{ url('images/product-category-inner.jpg') }}" alt="">
    </section>

    {{-- Bradcrum --}}
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                  <div class="page-breadcrumb">
                      <h2>{{$category->name}}</h2>
                      <ul class="breadcrumb">
                          <li><a href="{{ url('/') }}"><span class="fa fa-home"></span> Home</a></li>
                          <li class="active">{{$category->name}}</li>
                      </ul>
                  </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Containt --}}
    <section class="fine-jewellery">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>{{$category->name}}</h1>
                </div>
                <div class="col-sm-12">
                    <div class="fine-jewellery-box">
                            @php 
                                $count = 0;
                            @endphp
                                    
                            <div class="row">
                                @forelse($sub_category as $subcate)
                                    
                                    <div class="col-sm-6 col-md-4 col-xs-6">
                                        <div class="pro-box">
                                            <div class="img-part">
                                                <img src="@if($subcate->cover!=null) {{ url('storage/'.$subcate->cover) }} @endif" alt="">
                                            </div>
                                            <div class="text-pro-part">
                                                <h3>{{ $subcate->name }}</h3>
                                                <h4>{{ $category->name }}</h4>
                                                @if($subcate->slug=="")
                                                {{-- <a href="{{ url('category/'.$category->slug.'/'.$subcate->slug) }}" class="hvr-icon-wobble-horizontal">
                                                    BROWSE THE COLLECTION <i class="fa fa-angle-right hvr-icon"></i>
                                                </a> --}}
                                                @else
                                                    <a href="{{ url('category/'.$subcate->slug) }}" class="hvr-icon-wobble-horizontal">
                                                    BROWSE THE COLLECTION <i class="fa fa-angle-right hvr-icon"></i>
                                                </a>            
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @php $count++; @endphp
                                @empty
                                @endforelse
                            </div>
{{--                             <div class="col-sm-6">
                                <div class="pro-box pl-0 mt-130">
                                    <div class="img-part">
                                        <img src="{{ url('images/fine-jewellery/fine-jewellery1.jpg') }}" alt="">
                                    </div>
                                    <div class="text-pro-part">
                                        <h3>Antique</h3>
                                        <h4>Jewelry</h4>
                                        <a href="#" class="hvr-icon-wobble-horizontal">
                                            BROWSE THE COLLECTION <i class="fa fa-angle-right hvr-icon"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="pro-box pl-0">
                                    <div class="img-part">
                                        <img src="{{ url('images/fine-jewellery/fine-jewellery3.jpg') }}" alt="">
                                    </div>
                                    <div class="text-pro-part">
                                        <h3>Gold</h3>
                                        <h4>Jewelry</h4>
                                        <a href="#" class="hvr-icon-wobble-horizontal">
                                            BROWSE THE COLLECTION <i class="fa fa-angle-right hvr-icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pro-box mt-130 pr-0">
                                    <div class="img-part">
                                        <img src="{{ url('images/fine-jewellery/fine-jewellery2.jpg') }}" alt="">
                                    </div>
                                    <div class="text-pro-part">
                                        <h3>DIAMOND</h3>
                                        <h4>Jewelry</h4>
                                        <a href="#" class="hvr-icon-wobble-horizontal">
                                            BROWSE THE COLLECTION <i class="fa fa-angle-right hvr-icon"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="pro-box pr-0">
                                    <div class="img-part">
                                        <img src="{{ url('images/fine-jewellery/fine-jewellery4.jpg') }}" alt="">
                                    </div>
                                    <div class="text-pro-part">
                                        <h3>SIMON G</h3>
                                        <h4>Jewelry</h4>
                                        <a href="#" class="hvr-icon-wobble-horizontal">
                                            BROWSE THE COLLECTION <i class="fa fa-angle-right hvr-icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </div> --}}
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Subscripe --}}
    @include('front.subscribe')

    {{-- <div class="container">

        <hr>
        <div class="row">
            <div class="category-top col-md-12">
                <h2>{{ $category->name }}</h2>
                {!! $category->description !!}
            </div>
        </div>
        <hr>
        <div class="col-md-3">
            @include('front.categories.sidebar-category')
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="category-image">
                    @if(isset($category->cover))
                        <img src="{{ asset("storage/$category->cover") }}" alt="{{ $category->name }}" class="img-responsive" />
                    @else
                        <img src="https://placehold.it/1200x200" alt="{{ $category->cover }}" class="img-responsive" />
                    @endif
                </div>
            </div>
            <hr>
            <div class="row">
                @include('front.products.product-list', ['products' => $products])
            </div>
        </div>
    </div> --}}
@endsection
