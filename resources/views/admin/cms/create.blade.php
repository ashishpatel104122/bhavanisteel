@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage CMS</h4> </div>
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($cms->id))
								<li class="active"><a>Edit CMS</a></li>
							@else
								<li class="active"><a>Add CMS</a></li>
							@endif
							<li><a href="{{ route('admin.cms.list') }}">CMS Listing</a></li>
					  </ul>
					  <div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								  <form action="{{ route('admin.cms.store') }}" method="post" class="form" enctype="multipart/form-data">
									@csrf
		                			@if(isset($cms->id))
				            			<input type="hidden" name="id" value="{{$cms->id}}">
		            				@else
				            			<input type="hidden" name="id" value="">
			            			@endif
									<div class="row">
										<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Title<span class="red">*</span></label>
												<input type="text" class="form-control" name="title" id="title" value="@if(isset($cms->title)){{$cms->title}}@elseif(old('title')!=''){{old('title')}} @endif" maxlength="50" placeholder="Title">
												@if($errors->has('title'))
	                                      			<span class="text-danger">{{$errors->first('title')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Type<span class="red">*</span></label>
												<select name="type" id="type" class="form-control" style="width: 100%; height:36px;">
													<option value="">Type</option>
													<option value="about_us" @if(isset($cms->type) && $cms->type=="about_us") selected="true" @endif>About Us</option>
													<option value="contact_us" @if(isset($cms->type) && $cms->type=="contact_us") selected="true" @endif>Contact Us</option>
												</select>
												@if($errors->has('type'))
	                                      			<span class="text-danger">{{$errors->first('type')}}</span>
	                                    		@endif
											</div>
										</div>
									</div>
									<div class="row" id="description_row" style="display: none;">
										<div class="col-sm-4 col-md-8 col-sm-6 col-xs-12">
											<div class="form-group">
					                            <label for="description">Description </label>
					                            <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description" >@if(isset($cms->description)) {{ $cms->description}} @else{{ old('description') }} @endif</textarea>
					                        </div>
					                    </div>
					                </div>
					                <div id="contactus_row" style="display: none;">
						                <div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
						                            <label for="email">Email <span class="red">*</span></label>
						                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="@if(isset($contact_us->email)) {{ $contact_us->email}} @else{{ old('email') }} @endif">
						                		</div>
						                	</div>
						                	<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
						                            <label for="address">Address <span class="red">*</span></label>
						                            <textarea name="address" class="form-control" id="address" placeholder="Address">@if(isset($contact_us->address)) {{ $contact_us->address}} @else{{ old('address') }} @endif</textarea>
						                		</div>
						                	</div>
						                </div>
						                <div class="row">
						                	<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Phone No.</label>
						                			<table  id="phone_no" class="table">  
						                				@if(isset($contact_us) && $contact_us->phone_no!="")
							                				@php
							                					$phone = explode(',',$contact_us->phone_no);
							                				@endphp
							                				@foreach($phone as $key=> $phone)
							                				<tr id="row{{$key}}"> 
					                                            <td>
					                                                <input type="text" name="phone_no[]" placeholder="Enter Phone" class="form-control numeric" id="phone_no_name" maxlength="50" value="{{$phone}}">
					                                            </td>  
					                                            @if($key==0)  
					                                            <td  align="right"><button type="button" name="add_phone_no" id="add_phone_no" class="btn btn-success">Add +</button></td>  
					                                            @else
					                                            <td align="right"><button type="button" name="remove" id="{{$key}}" class="btn btn-danger btn_remove_phone_no">X</button></td>
					                                            @endif
					                                        </tr>
					                                        @endforeach
						                				@else
				                                        <tr> 
				                                            <td>
				                                                <input type="text" name="phone_no[]" placeholder="Enter Phone" class="form-control numeric" id="phone_no_name" maxlength="50" />
				                                            </td>    
				                                            <td  align="right"><button type="button" name="add_phone_no" id="add_phone_no" class="btn btn-success">Add +</button></td>  
				                                        </tr>  
						                				@endif
				                                    </table>
				                                </div>
				                            </div>
											
							            </div>
					                </div>
					                
									<div class="row">
										<div class="col-sm-12">
											<hr style="height: 1px; background: #e4e4e4;">
										</div>
										<div class="col-sm-12">
											<div class="submit-btn">
												<button type="submit" class="btn btn-submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
<script src="//cdn.ckeditor.com/4.15.1/full/ckeditor.js"></script>
<script type="text/javascript">
var i=1;
$(document).ready(function() {
	
	$("#type").on('change',function(){
        type = $("#type").val();
        getData(type);
    });
    if($("#type").val()!=""){
        type = $("#type").val();
    	getData(type)
    }
});
$(document).on('input','.numeric', function (event) {
        this.value = this.value.replace(/[^0-9\+.]/g, '');
    });
$(document).on('click', '.btn_remove_phone_no', function(){  
    var button_id = $(this).attr("id");   
    $('#row'+button_id+'').remove();  
});

$(document).on('click', '#add_phone_no', function(){ 
    i++;    
    $('#phone_no').append('<tr id="row'+i+'btn" style="padding-top:10px;" class="dynamic-added row'+i+'btn"><td><input type="text" name="phone_no[]" id="phone_no_name_'+i+'" required placeholder="Enter Phone" class="form-control numeric phone_no_txt_'+i+'" maxlength="50" data-addition-information-txt-id="'+i+'" /></td><td align="right"><button type="button" name="remove" id="'+i+'btn" class="btn btn-danger btn_remove_phone_no">X</button></td></tr>'); 
});    
function getData(type){
	$("#description_row").hide();
    $("#contactus_row").hide();
    if (type=="about_us") {
    	$("#description_row").show();
    }else if(type=="contact_us"){
    	$("#contactus_row").show();
    }
}
</script>
@endpush