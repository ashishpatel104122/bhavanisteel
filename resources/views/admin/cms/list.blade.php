@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage CMS</h4>
			</div>
		</div>
		@include('layouts.errors-and-messages')
		
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<!-- <li><a href="{{ route('admin.cms.create') }}">Add CMS</a></li> -->
							<li class="active"><a>CMS Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="category" class="table border-b1px" style="width:100%">
											<thead>
												<tr>
													<th>Title</th>
													<th>Type</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
											@forelse ($cms as $cms)
												<tr>
													<td>{{ $cms->title }}</td>
													<td>@if($cms->type=="about_us") {{  "About Us" }} @else {{ "Contact Us" }} @endif</td>
													<td>
														<div class="action-btn">
															<form action="{{ route('admin.cms.destroy', $cms->id) }}" method="post" class="form-horizontal">
																{{ csrf_field() }}
																<div class="btn-group">
																	<a href="{{ route('admin.cms.edit', $cms->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																	<button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																</div>
															</form>
														</div>
													</td>
												</tr>
											@empty
												<tr>
													<td colspan="3" align="center">Data Not Found</td>
												</tr>
											@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>
				  		</div>
					</div>
				</div>
			</div>
		</div>
	
</div>
@endsection
@section('css')
<style type="text/css">
	.btn-info, .btn-info.disabled {
        color: black!important;
        background: #e4e7ea!important;
    }
    .btn-info:hover{
        border:1px solid!important;
        border-color: none!important;
    }
</style>
@endsection
@section('js')
<script>
	$(document).ready(function() {
		var dataTable = $('#category').DataTable({
		});
		$("#date").change(function($e){

		date = $("#date").val();
		table = $("#category")[0].children[1].children;
		// dataTable.rows.table().data().add(date);
		 var data = dataTable.rows().table().data();
    	dataTable.draw();
	  //    	console.log(data);
		 // data.each(function (value, index) {
		 // 	// value = value[1].add('2');
	 	//     // value[1].('13');
	 	//     // value = value[1]+('123');
	 	//     // value[1].('13');
	 	    
		 // });
		// console.log(table);
		});
	} );
</script>
@endsection

