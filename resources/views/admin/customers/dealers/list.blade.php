@extends('layouts.admin-sol.app')
@section('content')

<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Manage Dealers</h4>
        </div>
    </div>
    @include('layouts.errors-and-messages')
    <div class="page-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="customer-tab">                          
                    <ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
                        <li><a  href="{{ route('admin.customers.index') }}">User Listing</a></li>
                        <li class="active"><a data-toggle="tab" href="">{{ $customer->name }}'s Dealers Listing</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="CustomerListing" class="tab-pane fade in active">
                            <div class="manage-customer">
                                <table id="customer" class="table border-b1px" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Dealer Name</th>
                                        <th>Mobile</th>
                                        <th>GST</th>
                                        <th>City</th>
                                        <th>Address</th>
                                        <th>Created Time</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{-- {{ dd($dealers) }} --}}
                                    @forelse ($dealers as  $dealer)
                                        <tr>
                                            <td>{{ $dealer->name }}</td>
                                            <td>{{ $dealer->mobile }}</td>
                                            @if(isset($dealer->gstno))
                                            <td>{{ $dealer->gstno }}</td>
                                            @elseif(isset($dealer->gst_image))
                                            <td>
                                                <a href="{{ url($dealer->gst_image) }}" target="_blank" >View Image</a>
                                            </td>
                                            @else
                                            <td></td>
                                            @endif
                                            <td>{{ $dealer->city }}</td>
                                            <td>{{ $dealer->address }}</td>
                                            <td>{{ date('d-m-Y H:i:s',strtotime($dealer->created_at))  }}</td>
                                            <td>
                                                <form action="{{ route('admin.dealers.destroy', $dealer->id) }}" method="post" class="form-horizontal">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="delete">
                                                    <div class="action-btn">
                                                        <button onclick="return confirm('Are you sure? You want to delete.')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="10" align="center">Data Not Found</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">
    .search{
        float: right!important;
        padding-bottom: 10px;
    }
    .btn-info, .btn-info.disabled {
        color: black!important;
        background: #e4e7ea!important;
    }
    .btn-info:hover{
        border:1px solid!important;
        border-color: none!important;
    }
</style>
@section('js')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $('#customer').DataTable({
           
        });
    } );
</script>
@endsection