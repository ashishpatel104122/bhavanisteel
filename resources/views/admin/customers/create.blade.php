@extends('layouts.admin-sol.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Manage Users</h4>
                </div>
            </div>
            @include('layouts.errors-and-messages')
            <div class="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="customer-tab">                          
                            <ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
                                <li><a href="{{ route('admin.customers.index') }}">Users Listing</a></li>
                                <li class="active"><a data-toggle="tab">Add User</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <form action="{{ route('admin.customers.store') }}" id="create_customer" enctype="multipart/form-data" method="post" class="form">
                                        <div class="box-body">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label for="user_id">User Id <span class="text-danger">*</span></label>
                                                <input type="text" name="user_id" id="user_id" placeholder="User Id" class="form-control" value="{{ $name }}" readonly="">
                                                @if ($errors->has('user_id'))
                                                    <span class="text-danger" style="color: red">{{ $errors->first('user_id') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email <span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <input type="text" name="email" id="email" placeholder="Email" class="form-control" value="{{ old('email') }}">
                                                </div>
                                                @if ($errors->has('email'))
                                                    <span class="text-danger" style="color: red">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password <span class="text-danger">*</span></label>
                                                <input type="text" name="password" id="password" value="{{ $password }}" class="form-control" readonly="">
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Mobile <span class="text-danger">*</span></label>
                                                <input type="text" name="phone" id="phone" placeholder="Mobile" class="form-control numeric" value="{{ old('phone') }}" maxlength="10">
                                                @if ($errors->has('phone'))
                                                    <span class="text-danger" style="color: red">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="role">Role </label>
                                                <select name="role" id="role" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="0">Dealer</option>
                                                    <option value="1">Executive</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="status">Active </label>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="0">Disable</option>
                                                    <option value="1">Enable</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.products.index') }}" class="btn btn-default">Back</a>
                                                <button type="submit" class="btn btn-primary">Create</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script type="text/javascript">
    $('#images').bind('change', function () {
        var filename = $("#images").val();
        if (/^\s*$/.test(filename)) {
            $(".file-upload").find('#images').removeClass('active');
            $("#noFile").text("No file chosen...");
        }
        else {
            $(".file-upload").find('#images').addClass('active');
            $("#noFileBackground").text(filename.replace("C:\\fakepath\\", ""));
        }
    });
    $("#create_customer").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            user_id: {
                required: true,
                maxlength: 25,
            },
            phone: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
            },
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 15,
            },
            status : {
                required: true,
            }
        },
        messages: {
            "user_id":{
                required: "Please enter name.",
                // lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character.",
            },
            "phone":{
                required: "Please enter mobile.",
                digits: "Please enter number only.",
                minlength: "Please enter min 10 digit.",
                maxlength: "Please enter max 10 digit.",
            },
            "email":{
                required: "Please enter email.",
                email: "Please enter a valid email address.",
            },
            "password":{
                required: "Please enter password.",
                minlength: "Please enter min 8 character.",
                maxlength: "Please enter max 15 character.",
            },
            "status":{
                required: "Please select status.",
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });

    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
</script>
@endsection

