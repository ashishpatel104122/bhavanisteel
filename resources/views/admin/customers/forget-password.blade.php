@extends('layouts.admin-sol.app')
@section('content')

<!-- @include('layouts.errors-and-messages') -->
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Request Password</h4>
        </div>
    </div>
    @include('layouts.errors-and-messages')
    <div class="page-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="customer-tab">                          
                    <ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
                        <li class="active"><a data-toggle="tab" href="#CustomerListing">Users Listing</a></li>
                        @php
                            $permission = App\Helpers\Permission::permission('request_password');
                        @endphp
                    </ul>
                    <div class="tab-content">
                        <div id="CustomerListing" class="tab-pane fade in active">
                            <div class="manage-customer">
                                <table id="customer" class="table border-b1px" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>User Id</th>
                                        <th>Name</th>
                                        <th>E-mail</th>
                                        <th>Role</th>
                                        <th>State</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($customers as  $customer)
                                        @php
                                            $dealer = App\Shop\Dealers\Dealer::where('parent_id',$customer->id)->where('status',0)->get();            
                                        @endphp
                                        <tr>
                                            <td>{{ $customer->user_id }}</td>
                                            <td>{{ $customer->name }}</td>
                                            <td>{{ $customer->email }}</td>
                                            @if($customer->role==1)
                                            <td>Executive</td>
                                            @else
                                            <td>Dealers</td>
                                            @endif
                                            <td>{{ $customer->state_name }}</td>
                                            <td>{{ $customer->phone }}</td>
                                            <td>
                                                <form action="{{ route('admin.customers.destroy', $customer->id) }}" method="post" class="form-horizontal">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="delete">
                                                    <div class="action-btn">
                                                        @if($permission->edit==1)
                                                        <a href="{{ route('admin.customers.edit', $customer->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                        @endif
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="10" align="center">Data Not Found</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">
    .search{
        float: right!important;
        padding-bottom: 10px;
    }
    .btn-info, .btn-info.disabled {
        color: black!important;
        background: #e4e7ea!important;
    }
    .btn-info:hover{
        border:1px solid!important;
        border-color: none!important;
    }
</style>
@section('js')
<script>
    $(document).ready(function() {
        $('#customer').DataTable({
           "ordering": false
        });
    } );
</script>
@endsection