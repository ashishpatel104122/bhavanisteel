@extends('layouts.admin-sol.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Manage Users</h4>
                </div>
            </div>
            @include('layouts.errors-and-messages')
            <div class="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="customer-tab">                          
                            <ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
                                <li><a href="{{ route('admin.customers.index') }}">Users Listing</a></li>
                                <li class="active"><a data-toggle="tab">Edit User</a></li>
                            </ul>
                            <div class="tab-content">
                                <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> -->
                                    <form action="{{ route('admin.customers.update', $customer->id) }}" method="post" id="edit_customer" method="POST" class="form" enctype="multipart/form-data" >
                                        <div class="box-body">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="put">
                                            <div class="row">
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="user_id">User Id <span class="text-danger">*</span></label>
                                                        <input type="text" name="user_id" id="user_id" placeholder="Name" class="form-control" value="{!!$customer->user_id ?: old('user_id') !!}" readonly="">
                                                        @if ($errors->has('user_id'))
                                                            <span class="text-danger" style="color: red">{{ $errors->first('user_id') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="email">Email <span class="text-danger">*</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">@</span>
                                                            <input type="text" name="email" id="email" placeholder="Email" class="form-control" value="{!! $customer->email ?: old('email')  !!}">
                                                        </div>
                                                        @if ($errors->has('email'))
                                                            <span class="text-danger" style="color: red">{{ $errors->first('email') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="password">Password <span class="text-danger"></span></label>
                                                        <input type="password" name="password" id="password" placeholder="xxxxx" class="form-control" maxlength="15">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="phone">Mobile <span class="text-danger">*</span></label>
                                                        <input type="text" name="phone" id="phone" placeholder="Mobile Number" class="form-control" value="{!! $customer->phone ?: old('phone')  !!}" maxlength="10">
                                                        @if ($errors->has('phone'))
                                                            <span class="text-danger" style="color: red">{{ $errors->first('phone') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="name">Name <span class="text-danger"></span></label>
                                                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{!!$customer->name ?: old('name') !!}" maxlength="25">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="organisation">Organisation <span class="text-danger"></span></label>
                                                        <input type="text" name="organisation" id="organisation" placeholder="Organisation" class="form-control" value="{!!$customer->organisation ?: old('organisation') !!}" maxlength="25">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="address_line1">Address 1 <span class="text-danger"></span></label>
                                                        <textarea name="address_line1" id="address_line1" placeholder="Address" class="form-control">{!!$customer->address_line1 ?: old('address_line1') !!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="address_line2">Address 2 <span class="text-danger"></span></label>
                                                        <textarea name="address_line2" id="address_line2" placeholder="Address" class="form-control">{!!$customer->address_line2 ?: old('address_line2') !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="country">Country <span class="text-danger"></span></label>
                                                        <input type="text" name="country" id="country" placeholder="Country" class="form-control" value="{!!$customer->country ?: old('country') !!}"  maxlength="30">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="country">State <span class="text-danger"></span></label>
                                                        <select name="state" id="state" class="form-control">
                                                            @if(!isset($customer->state))
                                                                <option value="">Select</option>
                                                            @endif
                                                            @foreach($states as $state)
                                                                <option value="{{$state->id}}" @if(isset($customer->state) && $customer->state==$state->id) selected @endif>{{$state->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="country">City <span class="text-danger"></span></label>
                                                        <input type="text" name="city" class="form-control" value="{!!$customer->city ?: old('city') !!}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="zip">Zip <span class="text-danger"></span></label>
                                                        <input type="text" name="zip" id="zip" placeholder="Zip" class="form-control" value="{!!$customer->post_code ?: old('zip') !!}" maxlength="6" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="role">Role <span class="text-danger">*</span></label>
                                                        <select name="role" id="role" class="form-control">
                                                            <option value="0" @if($customer->role == 0) selected="selected" @endif>Dealer</option>
                                                            <option value="1" @if($customer->role == 1) selected="selected" @endif>Executive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="status">Status <span class="text-danger">*</span></label>
                                                        <select name="status" id="status" class="form-control">
                                                            <option value="0" @if($customer->status == 0) selected="selected" @endif>Disable</option>
                                                            <option value="1" @if($customer->status == 1) selected="selected" @endif>Enable</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                {{-- <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="aadhar_card">Adhar Cart No <span class="text-danger"></span></label>
                                                        <input type="text" name="aadhar_card" id="aadhar_card" placeholder="Adhar Cart No" class="form-control" value="{!!$customer->aadhar_card ?: old('aadhar_card') !!}"  maxlength="12">
                                                    </div>
                                                </div> --}}
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="is_active">Active <span class="text-danger">*</span></label>
                                                        <select name="is_active" id="is_active" class="form-control">
                                                            <option value="0" @if($customer->is_active == 0) selected="selected" @endif>Disable</option>
                                                            <option value="1" @if($customer->is_active == 1) selected="selected" @endif>Enable</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                    <br>
                                                        <input name="is_restricted" type="checkbox" name="" value="0" class="" @if($customer->is_restricted==0) checked="" @endif>
                                                        Any Restriction 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-10 col-md-10 col-sm-10 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="zip">Notes <span class="text-danger"></span></label>
                                                        <textarea id="notes" name="notes" class="form-control">{!!$customer->notes ?: old('notes') !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="background_image">GST Image <span class="text-danger"></span></label>
                                                        <div class="file-upload background_image">
                                                            <div class="file-select">
                                                                <div class="file-select-button" id="fileName">Choose Image <span class="red">*</span></div>
                                                                <div class="file-select-name" id="noFileBackground">No file chosen...</div>
                                                                <input type="file" name="background_image" id="background_image" accept="image/*" value="@if(isset($customer->gst_image)){{$customer->gst_image}}@elseif(old('background_image')!=''){{old('background_image')}} @endif" >
                                                            </div>
                                                        </div>
                                                        @if($errors->has('background_image'))
                                                            <span class="text-danger">{{$errors->first('background_image')}}</span>
                                                        @endif
                                                    </div>
                                                    @if(isset($customer->gst_image)) 
                                                        <div class="row">
                                                            <div class="col-sm-10 col-md-10 col-sm-10 col-xs-12">
                                                                <div class="form-group">
                                                                    <img src="{{url($customer->gst_image) }}" height="100px" width="100px">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                                {{-- <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="gst_no">GST No <span class="text-danger"></span></label>
                                                        <input type="text" name="gst_no" id="gst_no" placeholder="GST No" class="form-control" value="{!!$customer->gst_no ?: old('gst_no') !!}"  maxlength="15">
                                                    </div>
                                                </div> --}}
                                                <div class="col-sm-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="images">Profile Image <span class="text-danger"></span></label>
                                                        <div class="file-upload images">
                                                            <div class="file-select">
                                                                <div class="file-select-button" id="fileName">Choose Image 
                                                                </div>
                                                                <div class="file-select-name" id="noFileProfile">No file chosen...</div>
                                                                <input type="file" name="images" id="images" accept="image/*" value="@if(isset($customer->profile)){{$customer->profile}}@elseif(old('images')!=''){{old('images')}} @endif" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if(isset($customer->profile)) 
                                                        <div class="row">
                                                            <div class="col-sm-10 col-md-10 col-sm-10 col-xs-12">
                                                                <div class="form-group">
                                                                    <img src="{{url($customer->profile) }}" height="100px" width="100px">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.customers.index') }}" class="btn btn-default btn-sm">Back</a>
                                                <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                            </div>
                                        </div>
                                    <!-- </form> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">

    $('#background_image').bind('change', function () {
        var filename = $("#background_image").val();
        if (/^\s*$/.test(filename)) {
            $(".file-upload").find('#background_image').removeClass('active');
            $("#noFile").text("No file chosen...");
        }
        else {
            $(".file-upload").find('#background_image').addClass('active');
            $("#noFileBackground").text(filename.replace("C:\\fakepath\\", ""));
        }
    });
     $('#images').bind('change', function () {
        var filename = $("#images").val();
        if (/^\s*$/.test(filename)) {
            $(".file-upload").find('#images').removeClass('active');
            $("#noFile").text("No file chosen...");
        }
        else {
            $(".file-upload").find('#images').addClass('active');
            $("#noFileProfile").text(filename.replace("C:\\fakepath\\", ""));
        }
    });

    $("#edit_customer").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            user_id: {
                required: true,
                maxlength: 25,
            },
            phone: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
            },
            email: {
                required: true,
                email: true,
            },
            status : {
                required: true,
            }
        },
        messages: {
            "user_id":{
                required: "Please enter name.",
                maxlength: "Please enter max 25 character.",
            },
            "phone":{
                required: "Please enter mobile.",
                digits: "Please enter number only.",
                minlength: "Please enter min 10 digit.",
                maxlength: "Please enter max 10 digit.",
            },
            "email":{
                required: "Please enter email.",
                email: "Please enter a valid email address.",
            },
            "status":{
                required: "Please select status.",
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });

    $('#state').change(function(){
        // $("#city option").remove();
        var id = $(this).val();
        $.ajax({
            url : "{{ route('admin.customers.getCity') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            dataType: 'json',
            success: function( data )
            {
                $('#city').empty();
                if(data) {
                    $('#city').append('<option value="">Select City</option>');
                    $.each(data, function(key, value) {
                        console.log(value);
                        $('#city').append('<option value=' + key + '>' + value + '</option>');
                    });
                }
            },
            error: function()
            {
                alert('error...');
            }
        });
    });

    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
</script>
@endsection

