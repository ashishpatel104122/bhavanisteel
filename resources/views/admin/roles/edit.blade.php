@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Venders</h4> </div>
			<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#">Manage Products</a></li>
					<li class="active">Add Products</li>
				</ol>
			</div> -->
			<!-- /.col-lg-12 -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					@include('layouts.errors-and-messages')
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<li><a href="{{ route('admin.employees.create') }}">Add Vender</a></li>
							<li><a href="{{ route('admin.employees.index') }}">Vendor's Listing</a></li>
							<li class="active"><a href="{{ route('admin.roles.index') }}">Roles</a></li>
							<li><a href="{{ route('admin.permissions.index') }}">Permissions</a></li>
					  </ul>
					  
					  <div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								  <form action="{{ route('admin.roles.update', $role->id) }}" method="post" class="form">
								  	<input type="hidden" name="_method" value="put">
										{{ csrf_field() }}
										<div class="row">
											<div class="col-lg-5 col-md-6 col-sm-6">
												<div class="form-group">
													<label for="display_name">Display Name <span class="text text-danger">*</span></label>
													<input type="text" name="display_name" id="display_name" placeholder="Display name" class="form-control" value="{{ old('display_name') ?: $role->display_name }}">
												</div>
											</div>
											<div class="col-lg-5 col-md-6 col-sm-6">
												<div class="form-group">
													<label for="description">Description</label>
													<textarea name="description" id="description" class="form-control ckeditor" placeholder="Description"> {!! old('description') ?: $role->description !!}</textarea>
												</div>
											</div>
											<div class="col-lg-10 col-md-12 col-sm-12">
												<div class="form-group">
													<label for="permissions">Permissions</label>
													<select name="permissions[]" id="permissions" class="form-control select2" multiple="multiple">
														@foreach($permissions as $permission)
															<option @if(in_array($permission->id, $attachedPermissionsArrayIds)) selected="selected" @endif value="{{ $permission->id }}">{{ $permission->display_name }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="row">
										<div class="col-sm-12">
											<hr style="height: 1px; background: #e4e4e4;">
										</div>
										<div class="col-sm-12">
											<div class="submit-btn">
												<button type="submit" class="btn btn-submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection