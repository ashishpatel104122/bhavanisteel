@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Venders</h4> </div>
			<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#">Manage Products</a></li>
					<li class="active">Add Products</li>
				</ol>
			</div> -->
			<!-- /.col-lg-12 -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<li><a href="{{ route('admin.employees.create') }}">Add Vender</a></li>
							<li><a href="{{ route('admin.employees.index') }}">Vendor's Listing</a></li>
							<li><a href="{{ route('admin.roles.index') }}">Roles</a></li>
							@if(isset($roles->id))
							<li class="active"><a href="#" class="">Edit Roles</a></li>
							@else
							<li class="active"><a href="{{ route('admin.roles.create') }}" class="">Add Roles</a></li>
							@endif
					  </ul>
					  <div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
									<form action="{{ route('admin.roles.store') }}" method="post" class="form">
										<div class="box-body">
											{{ csrf_field() }}
											<div class="row">
												<input type="hidden" name="id" value="@if(isset($roles->id)){{$roles->id}}@endif">
												<div class="col-lg-5 col-md-6 col-sm-6">
													<div class="form-group">
														<label for="display_name"> Name</label>
														<input type="text" name="name" id="display_name" placeholder="Name" class="form-control" maxlength="100" value="@if(isset($roles->name)){{$roles->name}}@elseif(old('name')!=''){{old('name')}} @endif">
														@if($errors->has('name'))
					                                    	<span class="text-danger">{{$errors->first('name')}}</span>
					                                    @endif
													</div>
												</div>
												<div class="col-lg-5 col-md-6 col-sm-6">
													<div class="form-group">
														<label for="description">Description</label>
														<textarea name="description" id="description" class="form-control ckeditor" maxlength="500" placeholder="Description">@if(isset($roles->description)){{$roles->description }}@elseif(old('description')!=''){{old('description')}} @endif</textarea>
														@if($errors->has('description'))
					                                    	<span class="text-danger">{{$errors->first('description')}}</span>
					                                    @endif
													</div>
												</div>
												<div class="col-lg-10 col-md-12 col-sm-12">
												<div class="form-group">
													<label for="permissions">Permissions</label>
													{{-- <select name="permissions[]" id="permissions" class="form-control select2" multiple="multiple">
													@foreach($permissions as $permission)
														<option @if(isset($permission_role))
														@foreach($permissions_role as $permission_role)
													 		@if($permission_role->permission_id == $permission->id ) selected= "selected" @endif value="{{ $permission->id }}"
											 		 	@endforeach
											 		 	@endif
											 		 	value="{{$permission->id}}"
											 		 	>
										 		 	 	{{$permission->name}}</option>
													@endforeach
													</select>
													@if($errors->has('permissions'))
				                                    	<span class="text-danger">{{$errors->first('permissions')}}</span>
				                                    @endif --}}
												</div>
											</div>
										</div>
										<div class="row">
				                            <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
				                                <div class="form-group">
				                                    <table  class="table table-striped" id="permission">
				                                        <thead>
				                                            <th>Sr No</th>
				                                            <th>Modules</th>
				                                            <th> View <input type="checkbox" onClick="viewAll(this)" class="role-check-box"></th>
				                                            <th> Add <input type="checkbox" onClick="addAll(this)" class="role-check-box"></th>
				                                            <th> Edit <input type="checkbox" onClick="editAll(this)" class="role-check-box"></th>
				                                            <th> Delete <input type="checkbox" onClick="deleteAll(this)" class="role-check-box"></th>
				                                        </thead>
				                                        <tbody>
			                                            @foreach($modules as $module)
			                                                <tr>
			                                                    <td><input type="hidden" name="module_id[]" value="{{ $module->id }}"> {{ $module->id }} </td>
			                                                    <td>{{ $module->display_name }} </td>
			                                                    <td align="center"><input type="checkbox" name="view[]" value="1-{{$module->name}}" 
		                                                    	@if(isset($permissions_roles))
		                                                    	@foreach($permissions_roles as $permissions_role)  		@if($permissions_role->module_id==$module->id && $permissions_role->view==1)
		                                                    	 	checked 
		                                                    		@endif
			                                                    @endforeach
			                                                    @endif></td>
			                                                    <td align="center"><input type="checkbox" name="add[]" value="1-{{$module->name}}"
		                                                    	@if(isset($permissions_roles))
		                                                    	@foreach($permissions_roles as $permissions_role)  		@if($permissions_role->module_id==$module->id && $permissions_role->add==1)
		                                                    	 	checked 
		                                                    		@endif
			                                                    @endforeach
			                                                    @endif></td>
			                                                    <td align="center"><input type="checkbox" name="edit[]" value="1-{{$module->name}}"
		                                                    	@if(isset($permissions_roles))
		                                                    	@foreach($permissions_roles as $permissions_role)  		@if($permissions_role->module_id==$module->id && $permissions_role->edit==1)
		                                                    	 	checked 
		                                                    		@endif
			                                                    @endforeach
				                                                @endif></td>
			                                                    <td align="center"><input type="checkbox" name="delete[]" value="1-{{$module->name}}"
			                                                    @if(isset($permissions_roles))
		                                                    	@foreach($permissions_roles as $permissions_role)  		@if($permissions_role->module_id==$module->id && $permissions_role->delete==1)
		                                                    	 	checked 
		                                                    		@endif
			                                                    @endforeach
			                                                    @endif></td>
			                                                </tr>
		                                        	    @endforeach   
				                                        </tbody>
				                                    </table>
				                                </div>
				                            </div>
				                        </div>
										<div class="row">
											<div class="col-sm-12">
												<hr style="height: 1px; background: #e4e4e4;">
											</div>
											<div class="col-sm-12">
												<div class="submit-btn">
													<button type="submit" class="btn btn-submit">Submit</button>
												</div>
											</div>
										</div>
									</form>
								</div>
					 	 	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
@push('js')
<script type="text/javascript">

    function addAll(source) {
        checkboxes = document.getElementsByName('add[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
 	function viewAll(source) {
        checkboxes = document.getElementsByName('view[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    function editAll(source) {
        checkboxes = document.getElementsByName('edit[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    function deleteAll(source) {
        checkboxes = document.getElementsByName('delete[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
</script>