@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Vendors</h4> </div>
			<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#">Manage Products</a></li>
					<li class="active">Add Products</li>
				</ol>
			</div> -->
			<!-- /.col-lg-12 -->
		</div>
		@include('layouts.errors-and-messages')
		@if(!$roles->isEmpty())
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@php
	                            $permission = App\Helpers\Permission::permission('roles');
	                        @endphp
							@if($permission->add==1)
							<li><a href="{{ route('admin.employees.create') }}">Add Vender</a></li>
							@endif
							@if($permission->view==1)
							<li><a href="{{ route('admin.employees.index') }}">Vendor's Listing</a></li>
							@endif
							<li class="active"><a>Roles</a></li>
	                        @if($permission->add==1)
							<li><a href="{{ route('admin.roles.create') }}" class="">Add Roles</a></li>
							@endif
					  	</ul>
				  		<div class="tab-content">
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="example" class="table border-b1px" style="width:100%">
											<thead>
												<tr>
													<td>Display Name</td>
													<td>Description</td>
													<td>Actions</td>
												</tr>
											</thead>
											<tbody>
											@foreach ($roles as $role)
												<tr>
													<td>
														{{ $role->name }}
													</td>
													<td>
														{!! $role->description !!}
													</td>
													<td>
														<div class="action-btn">
															<form action="{{ route('admin.roles.destroy', $role->id) }}" method="post" class="form-horizontal">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<div class="btn-group">
	                        									@if($permission->edit==1)
																	<a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																@endif
																@if($permission->delete==1)
																	<button onclick="return confirm('Are you sure? You want to delet.')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																@endif
																</div>
															</form>
														</div>
													</td>
												</tr>
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
					  </div>

					</div>
				</div>
			</div>
		</div>
		@endif
	</div>
@endsection
@section('js')
<script>
	$(document).ready(function() {
		$('#example').DataTable();
	} );
</script>
@endsection
