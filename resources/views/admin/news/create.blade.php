@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage News</h4> 
			</div>
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($news->id))
								<li class="active"><a>Edit News</a></li>
							@else
								<li class="active"><a>Add News</a></li>
							@endif
							<li><a href="{{ route('admin.news.list') }}">News Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								   	<form id="create_product" action="{{ route('admin.news.store') }}" method="post" class="form" enctype="multipart/form-data">
									{{ csrf_field() }}
									@if(isset($news->id))
				            			<input type="hidden" name="id" value="{{$news->id}}">
		            				@else
				            			<input type="hidden" name="id" value="">
			            			@endif
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Category<span class="red">*</span></label>
													<!--  -->
													<select name="category_id" id="category_id" class="form-control" style="width: 100%; height:36px;">
														<option value="">Select Category</option>
														@foreach($categories as $data)
															<option value="{{ $data->id }}"
															@if(old('category_id') == $data->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($news->category_id) && $data->id== $news->category_id))
					                                            selected="true"
					                                        @endif
					                                        >{{ $data->name }}</option>
					                                    @endforeach
													</select>
													@if($errors->has('category_id'))
		                                      			<span class="text-danger">{{$errors->first('category_id')}}</span>
		                                    		@endif
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group">
												<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<label>Name<span class="red">*</span></label>
													<input type="text" class="form-control" name="name" placeholder="Enter Name" id="name" value="@if(isset($news->name)){{$news->name}}@elseif(old('name')!=''){{old('name')}} @endif" maxlength="50">
													@if($errors->has('name'))
                                      					<span class="text-danger">{{$errors->first('name')}}</span>
                                    				@endif
                                    			</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="form-group">
												<div class="col-sm-4">
													<label>Size<span class="red">*</span></label>
													<input type="text" name="size" placeholder="Enter Size" class="form-control" maxlength="50" value="@if(isset($news->size)){{$news->size}}@elseif(old('size')!=''){{old('size')}} @endif">
												@if($errors->has('size'))
	                                      			<span class="text-danger">{{$errors->first('size')}}</span>
	                                    		@endif
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-sm-4">
												<div class="file-upload">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Choose Backgorund Image <span class="red">*</span></div>
														<div class="file-select-name" id="noFile">No file chosen...</div>
														<input type="file" name="background_image" id="cover" accept="image/*" value="@if(isset($news->background_image)){{$news->background_image}}@elseif(old('background_image')!=''){{old('background_image')}} @endif" >
													</div>
													@if(isset($news->background_image))
														<img src="{{url($news->background_image) }}" height="100px" width="100px">
													@endif
												</div>
												@if($errors->has('background_image'))
	                                      			<span class="text-danger">{{$errors->first('background_image')}}</span>
	                                    		@endif
											</div>
										</div>
										<br>
										<div class="row" style="margin-top: 10px">
											<div class="col-sm-12">
												<div class="submit-btn">
													<button type="submit" class="btn btn-submit">Submit</button>
												</div>
											</div>
										</div>
								 	</form>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
<style type="text/css">
	.file-select-name{
	    display: contents!important;
	}
</style>
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}

@push('js')
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
<!--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>-->
<script type="text/javascript">
$(document).ready(function() {
	$('#cover').bind('change', function () {
		var filename = $("#cover").val();
		if (/^\s*$/.test(filename)) {
			$(".file-upload").find('#cover').removeClass('active');
			$("#noFile").text("No file chosen...");
		}
		else {
			console.log(filename);
			$(".file-upload").find('#cover').addClass('active');
			$("#noFile").text(filename.replace("C:\\fakepath\\", ""));
		}
	});
});
</script>
@endpush