@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage news</h4> </div>
		</div>
		@include('layouts.errors-and-messages')
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
						 	@php
	                            $permission = App\Helpers\Permission::permission('news');
	                        @endphp
	                        @if($permission->add==1)
							<li><a href="{{ route('admin.news.create') }}">Add News</a></li>
							@endif
							<li class="active"><a>News Listing</a></li>
					  	</ul>
					  
					  	<div class="tab-content">
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="news" class="table border-b1px" style="width:100%">
											<thead>
												<tr>
													<th style="width: 15%">Name</th>
													<th style="width: 15%">Background Image</th>
													<th style="width: 15%">Category Name</th>
													<th style="width: 10%">Size</th>
													<th style="width: 10%">Status</th>
													<th style="width: 10%">Actions</th>
												</tr>
											</thead>
											<tbody>
												@forelse ($news as $data)
												<tr>
													<td>{{ $data->name }}</td>
													<td>
														<img src="{{url($data->background_image)}}" height="100px" width="100px">
													</td>
													<td>{{ $data->category_name }}</td>
													<td>{{ $data->size }}</td>
													<td>
													<form action="{{ route('admin.news.status',$data->id) }}" method="post" class="form-horizontal">
			                                                {{ csrf_field() }}
			                                                @if($data->status == "1")
			                                                    <button class="btn in-active-status" onclick="return confirm('Are you sure? You want to change status.')">Active</button>
			                                                @else
			                                                    <button class="btn in-active-status" onclick="return confirm('Are you sure? You want to change status.')">InActive</button>
			                                                @endif
			                                            </form>
			                                        </td>
													<td>
														<div class="action-btn">
															<form action="{{ route('admin.news.destroy', $data->id) }}" method="get" class="form-horizontal">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<div class="btn-group">
																	@if($permission->edit==1)
																	<a href="{{ route('admin.news.edit', $data->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																	@endif
																	@if($permission->delete==1)
																	<button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																	@endif
																</div>
															</form>
														</div>
													</td>
												</tr>
												@empty
												<tr>
													<td colspan="5" align="center">Data Not Found</td>
												</tr>
												@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
<script>
	$(document).ready(function() {
		$('#news').DataTable();
	} );
</script>
@endsection