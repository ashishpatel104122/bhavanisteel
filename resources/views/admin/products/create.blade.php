@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Products</h4> 
			</div>
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($products->id))
								<li class="active"><a>Edit Product</a></li>
							@else
								<li class="active"><a>Add Product</a></li>
							@endif
							<li><a href="{{ route('admin.products.index') }}">Product Listing</a></li>
					  	</ul>

					  	<div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								   	<form id="create_product" action="{{ route('admin.products.store') }}" method="POST" class="form" enctype="multipart/form-data" id="add_product">
									{{ csrf_field() }}
									@if(isset($products->id))
				            			<input type="hidden" name="id" value="{{$products->id}}">
		            				@else
				            			<input type="hidden" name="id" value="">
			            			@endif
			            				<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Select Category<span class="red">*</span></label>
													<select name="category_id" id="category_id" class="form-control" style="width: 100%; height:36px;">
														<option value="">Select Category</option>
														@foreach($categories as $data)
															<option value="{{ $data->id }}"
															@if(old('category_id') == $data->id)
	                                						selected="true"
	                                						@endif
					                                        @if(isset($products->category_id) && $data->id== $products->category_id))
					                                            selected="true"
					                                        @endif
					                                        >{{ $data->name }}</option>
					                                    @endforeach
													</select>
													@if ($errors->has('category_id'))
							                            <span class="text-danger" style="color: red">{{ $errors->first('category_id') }}</span>
							                        @endif
												</div>
											</div>
											<input type="hidden" name="selected_category" id="selected_category" value="@if(isset($products->category_id)){{$products->category_id}}@elseif(old('category_id')!=''){{old('category_id')}} @endif">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group" id="sub_cat" style="display: none;">
													<label>Select Sub Category<span class="red">*</span></label>
													<select name="sub_category_id" id="sub_category_id" class="form-control" style="width: 100%; height:36px;">
														<option value="">Select Sub Category</option>
													</select>													
		                                    		<label class="categery-error">Please select category id</label>
												</div>
											</div>
											<input type="hidden" name="selected_subcategory" id="selected_subcategory" value="@if(isset($products->sub_category_id)){{$products->sub_category_id}}@elseif(old('sub_category_id')!=''){{old('sub_category_id')}} @endif">
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Product Name<span class="red">*</span></label>
													<input type="text" class="form-control" name="name" placeholder="Enter Product Name" id="name" value="@if(isset($products->name)){{$products->name}}@elseif(old('name')!=''){{old('name')}} @endif" maxlength="50">
													@if($errors->has('name'))
                                      					<span class="text-danger">{{$errors->first('name')}}</span>
                                    				@endif
                                    				<label class="name-error">Please enter name</label>
												</div>
											</div>
											<div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
												<div class="form-group">
													<label>Status<span class="red">*</span></label>
													<select name="status" class="form-control">
														<option value="1" @if(isset($products->status) && 1== $products->status))
					                                            selected="true"
					                                        @endif>Active</option>
														<option value="0" @if(isset($products->status) && 0== $products->status))
					                                            selected="true"
					                                        @endif>InActive</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 col-md-8 col-sm-6 col-xs-12">
												<div class="form-group">
						                            <label for="description">Description </label>
						                            <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description" >@if(isset($products->description)) {!! $products->description!!} @else{{ old('description') }} @endif</textarea>
						                        </div>
						                    </div>
						                </div>
						                <div class="row">
											<div class="col-sm-4 col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
						                            <label for="description">Price </label>
						                            <input type="text" name="price" class="form-control numeric" placeholder="Price" value="@if(isset($products->price)){{$products->price}}@elseif(old('price')!=''){{old('price')}} @endif">
						                        </div>
						                    </div>
						                </div>
										<div class="row">
											<div class="col-sm-4 col-md-8 col-sm-6 col-xs-12">
						                		<div class="form-group">
													<label>Table Data</label>
						                			<table  id="phone_no" class="table">  
						                				@if(isset($products) && $products->key!="")
							                				@php
							                					$key_arrray = explode(',',$products->key);
							                					$value_array = explode(',',$products->value);
							                				@endphp
							                				@foreach($key_arrray as $key=> $value)
							                				<tr id="row{{$key}}"> 
					                                            <td>
					                                                <input type="text" name="key[]" placeholder="Enter Key" class="form-control numeric" id="phone_no_name" maxlength="50" value="{{$value}}">
					                                            </td>
					                                            <td>
					                                                <input type="text" name="value[]" placeholder="Enter Value" class="form-control numeric" id="phone_no_name" maxlength="50" value="{{$value_array[$key]}}">
					                                            </td>    
					                                            @if($key==0)  
					                                            <td  align="right"><button type="button" name="add_phone_no" id="add_phone_no" class="btn btn-success">Add +</button></td>  
					                                            @else
					                                            <td align="right"><button type="button" name="remove" id="{{$key}}" class="btn btn-danger btn_remove_phone_no">X</button></td>
					                                            @endif
					                                        </tr>
					                                        @endforeach
						                				@else
				                                        <tr> 
				                                            <td>
				                                                <input type="text" name="key[]" placeholder="Enter Key" class="form-control numeric" id="phone_no_name" maxlength="50" />
				                                            </td>    
				                                            <td>
				                                                <input type="text" name="value[]" placeholder="Enter Value" class="form-control numeric" id="phone_no_name" maxlength="50" />
				                                            </td>
				                                            <td  align="right"><button type="button" name="add_phone_no" id="add_phone_no" class="btn btn-success">Add +</button></td>  
				                                        </tr>  
						                				@endif
				                                    </table>
				                                </div>
				                            </div>
				                         </div>
						                <div class="row">
											<div class="col-sm-3">
												<div class="file-upload background_image">
													<div class="file-select">
														<div class="file-select-button" id="fileName">Choose Image <span class="red">*</span></div>
														<div class="file-select-name" id="noFileBackground">No file chosen...</div>
														<input type="file" multiple="" name="background_image[]" id="background_image" accept="image/*" value="@if(isset($products->background_image)){{$products->background_image}}@elseif(old('background_image')!=''){{old('background_image')}} @endif" >
													</div>
													@if(isset($products_images) && $products_images!="")
														<div class="row">
														@foreach($products_images as $products_image)
															<div class="col-lg-3 col-md-2 col-sm-6 col-xs-12" style="margin-right: 10%;">
																<div class="form-group">
																	<img src="{{url($products_image->image) }}" height="100px" width="100px" id="background_image_change">
																	<br>
																	<a class="btn btn-danger" href="{{route('admin.product.remove.thumb',$products_image->id)}}">Delete</a>
																</div>
															</div>
														@endforeach
														</div>
													@endif
													<img id="image" src="" style="display: none;" height="100px" width="100px">
												</div>
												@if($errors->has('background_image'))
	                                      			<span class="text-danger">{{$errors->first('background_image')}}</span>
	                                    		@endif
											</div>
										</div>
										<div class="row" style="margin-top: 10px">
											<div class="col-sm-12">
												<div class="submit-btn">
													<button type="submit" class="btn btn-submit" id="submitbtn">Submit</button>
												</div>
											</div>
										</div>
								 	</form>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('css')	
<style type="text/css">
	.branch-error, .categery-error, .name-error, .design_no-error, .cover-error, .size-error, .quantity-error, .square-error,.boxsquare-error {
	  display: none;
	  color: #f95959 !important;
	  font-size: 12px;
	  position: absolute;
	}
	.errors_cover{
		color: red;
	}	
	.product_image{
		height: 72px;
		width: 82px;
	}
	.file-select-name{
		display: contents!important;
	}
	.show-image {
	    position: relative;
	    float:left;
	    margin:12px;
	}
	.show-image:hover img{
	    opacity:0.5;
	}
	.show-image:hover a.delete {
	    display: block;
	}
	.show-image a.delete {
	    position:absolute;
	    display:none;
	}
	.show-image a.delete {
			color: red;
			width: 100%;
	    	top:30%;
	}
	.first{
		display: none!important;	
	}
	.addtional_value_error{
		display: none;
		color: red!important;
	}
	.addtional_name_error{
		display: none;
		color: red!important;	
	}
	.addtional_value_array_error{
		display: none;
		color: red!important;
	}
	.addtional_name_array_error{
		display: none;
		color: red!important;	
	}
</style>
@endpush


@push('js')
<script src="//cdn.ckeditor.com/4.15.1/full/ckeditor.js"></script>
<script type="text/javascript">
var i=1;
$(document).ready(function() {
	$(".numeric").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	$(document).on('click', '#add_phone_no', function(){ 
	    i++;    
	    $('#phone_no').append('<tr id="row'+i+'btn" style="padding-top:10px;" class="dynamic-added row'+i+'btn"><td><input type="text" name="key[]" id="phone_no_name_'+i+'" required placeholder="Enter Key" class="form-control numeric phone_no_txt_'+i+'" maxlength="50" data-addition-information-txt-id="'+i+'" /></td><td><input type="text" name="value[]" id="phone_no_name_'+i+'" required placeholder="Enter Value" class="form-control numeric phone_no_txt_'+i+'" maxlength="50" data-addition-information-txt-id="'+i+'" /></td><td align="right"><button type="button" name="remove" id="'+i+'btn" class="btn btn-danger btn_remove_phone_no">X</button></td></tr>'); 
	}); 
    $('.numericWithStar').on('input', function (event) {
    	this.value = this.value.replace(/[^0-9\*]/g, '');
	});
	$(document).on('click', '.btn_remove_phone_no', function(){  
	    var button_id = $(this).attr("id");   
	    $('#row'+button_id+'').remove();  
	});

	$("#category_id").on('change',function(){
        category_id = $("#category_id").val();
        console.log(category_id);
        getSubCat(category_id);
    });
    selected_category = $("#selected_category").val();
    if (selected_category!=0) {
    	getSubCat(selected_category);
    }
	$("#background_image").change(function() {
	  // readURL(this);
	});
})
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
    	$("#image").show();
    	$("#background_image_change").hide();
      	$('#image').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function getSubCat(category_id){
    selected_subcategory = $("#selected_subcategory").val();
    $("#sub_category_id").empty();
    $("#sub_cat").hide();
    if(category_id!=null){
        $.ajax({
            url:"{{ url('admin/getcategory') }}",
            type:"POST",             
            data:{
            	_token:     '{{ csrf_token() }}',
            	category_id:category_id,
            	selected_subcategory:selected_subcategory
            },
            success:function(data) {
            	$('#sub_category_id').append('<option>Select Sub Category</option>');
                $.each( data, function(k, v) {
                	$("#sub_cat").show();
                	console.log(selected_subcategory);
                	if(selected_subcategory==k){
						$('#sub_category_id').append('<option value="'+ k +'" selected="true">' + v + '</option>');
					} else{
						$('#sub_category_id').append('<option value="'+ k +'">' + v + '</option>');
					}
                });
            },
            error: function(error){
            }
        });
    }
}
</script>
@endpush