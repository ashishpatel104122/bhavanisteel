@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Products</h4> </div>
		</div>
		@include('layouts.errors-and-messages')
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<li><a href="{{ route('admin.products.create') }}">Add Products</a></li>
							<li class="active"><a>Product Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="products" class="table border-b1px" style="width:100%">
											<thead>
												<tr>
													<th style="width: 15%">Category</th>
													<th style="width: 15%">Sub Category</th>
													<th style="width: 15%">Product Name</th>
													<th style="width: 15%">Image</th>
													<th style="width: 15%">Status</th>
													<th style="width: 10%">Actions</th>
												</tr>
											</thead>
											
											<tbody>
												@forelse ($products as $product)
													@php
														$subCat = DB::table('categories')->where('id',$product->sub_category_id)->first();
													@endphp
													<td>{{ $product->categories_name }}</td>
													<td>@if($subCat) {{ $subCat->name }} @endif</td>
													<td>{{ $product->name }}</td>
													<td>
														@if($product->background_image!="")
															<img src="{{url($product->background_image) }}" alt="" height="60px" width="60px" class="">
														@endif
													</td>
													<td>
														@if($product->status)
															<button class="form-control" style="background-color: green;color: black"> Active </button>
														@else
															<button class="form-control" style="background-color: red;color: black"> In Active </button>
														@endif
													</td>
													<td>
														<div class="action-btn">
															<form action="{{ route('admin.products.destroy', $product->id) }}" method="post" class="form-horizontal">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<div class="btn-group">
																	<a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																	<button onclick="return confirm('Are you sure you want to delete?')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																</div>
															</form>
														</div>
													</td>
												</tr>
												
												@empty
													<tr>
														<td colspan="8" align="center">Data Not Found</td>
													</tr>
												@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
{{-- <style type="text/css">
.btn-info, .btn-info.disabled {
    color: black!important;
    background: #e4e7ea!important;
}
.btn-info:hover{
    border:1px solid!important;
    border-color: none!important;
}
</style> --}}
@section('js')
<script>
	$(document).ready(function() {
		$('#products').DataTable();
	} );
</script>
@endsection