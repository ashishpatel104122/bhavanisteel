@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Categories</h4>
			</div>
		</div>
		@include('layouts.errors-and-messages')
		@if($categories)
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-listing">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							<li><a href="{{ route('admin.categories.create') }}">Add Category</a></li>
							<li class="active"><a>Categories Listing</a></li>
					  	</ul>
					  	<div class="tab-content">
					  		
							<div id="ProductListing" class="tab-pane fade in active">
								<div class="product-listing">
									<div class="table-responsive">
										<table id="category" class="table border-b1px" style="width:100%">
											<thead>
												<tr>
													<th>Name</th>
													<th>Parent Category</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
											@forelse ($categories as $category)
												@php
													$parentcat = DB::table('categories')->where('id',$category->parent_id)->first();
												@endphp
												<tr>
													<td>{{ $category->name }}</td>
													<td>@if($parentcat) {{ $parentcat->name }} @endif</td>
													<td>
														<div class="action-btn">
															<form action="{{ route('admin.categories.destroy', $category->id) }}" method="post" class="form-horizontal">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<div class="btn-group">
																	<a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-edit"><i class="mdi mdi-pencil"></i></a>
																	<button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-delete"><i class="mdi mdi-close"></i></button>
																</div>
															</form>
														</div>
													</td>
												</tr>
											@empty
												<tr>
													<td colspan="3" align="center">Data Not Found</td>
												</tr>
											@endforelse
											</tbody>
										</table>
								</div>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
	@endif
</div>
@endsection
@section('css')
<style type="text/css">
	.btn-info, .btn-info.disabled {
        color: black!important;
        background: #e4e7ea!important;
    }
    .btn-info:hover{
        border:1px solid!important;
        border-color: none!important;
    }
</style>
@endsection
@section('js')
<script>
	$(document).ready(function() {
		var dataTable = $('#category').DataTable({
		});
		$("#date").change(function($e){

		date = $("#date").val();
		table = $("#category")[0].children[1].children;
		// dataTable.rows.table().data().add(date);
		 var data = dataTable.rows().table().data();
    	dataTable.draw();
	  //    	console.log(data);
		 // data.each(function (value, index) {
		 // 	// value = value[1].add('2');
	 	//     // value[1].('13');
	 	//     // value = value[1]+('123');
	 	//     // value[1].('13');
	 	    
		 // });
		// console.log(table);
		});
	} );
</script>
@endsection

