@extends('layouts.admin-sol.app')
@section('content')
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Manage Categories</h4> </div>
			<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#">Manage Products</a></li>
					<li class="active">Add Products</li>
				</ol>
			</div> -->
			<!-- /.col-lg-12 -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-sm-12">
					
					<div class="product-tab">
						<ul class="nav nav-tabs" data-spy="affix" data-offset-top="135">
							@if(isset($categories->id))
								<li class="active"><a>Edit Category</a></li>
							@else
								<li class="active"><a>Add Category</a></li>
							@endif
							<li><a href="{{ route('admin.categories.index') }}">Categories Listing</a></li>
					  </ul>
					  <div class="tab-content">
							<div id="AddProcucts" class="tab-pane fade in active">
								<div class="add-product">
								  <form action="{{ route('admin.categories.store') }}" method="post" class="form" enctype="multipart/form-data">
									@csrf
		                			@if(isset($categories->id))
				            			<input type="hidden" name="id" value="{{$categories->id}}">
		            				@else
				            			<input type="hidden" name="id" value="">
			            			@endif
			            			<div class="row">
			            				<div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
											<div class="form-group">
												<label>Select Category</label>
												<select name="parent_id" id="parent_id" class="form-control" style="width: 100%; height:36px;">
													<option value="">Select Category</option>
													@foreach($parent_categorys as $data)
														<option value="{{ $data->id }}"
														@if(old('parent_id') == $data->id)
                                						selected="true"
                                						@endif
				                                        @if(isset($categories->parent_id) && $data->id== $categories->parent_id))
				                                            selected="true"
				                                        @endif
				                                        
				                                        >{{ $data->name }}</option>
				                                    @endforeach
												</select>
												@if($errors->has('parent_id'))
	                                      			<span class="text-danger">{{$errors->first('parent_id')}}</span>
	                                    		@endif
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
											<div class="form-group">
												<label>Name<span class="red">*</span></label>
												<input type="text" class="form-control" name="name" id="name" value="@if(isset($categories->name)){{$categories->name}}@elseif(old('name')!=''){{old('name')}} @endif" maxlength="50" placeholder="Enter Category Name">
												@if($errors->has('name'))
	                                      			<span class="text-danger">{{$errors->first('name')}}</span>
	                                    		@endif
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<hr style="height: 1px; background: #e4e4e4;">
										</div>
										<div class="col-sm-12">
											<div class="submit-btn">
												<button type="submit" class="btn btn-submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
<script type="text/javascript">
$(document).ready(function() {
	$('#cover').bind('change', function () {
		var filename = $("#cover").val();
		if (/^\s*$/.test(filename)) {
			$(".file-upload").find('#cover').removeClass('active');
			$("#noFile").text("No file chosen...");
		}
		else {
			$(".file-upload").find('#cover').addClass('active');
			$("#noFile").text(filename.replace("C:\\fakepath\\", ""));
		}
	});
});
</script>
@endpush