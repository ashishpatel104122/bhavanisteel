@extends('customers.layout.default')
@section('title','About Us | Jay Bhavani Steel')
@section('content')
<main class="innerPage" id="aboutus-main">
        <section class="section">
            <div class="container">
                <div class="heading"> <h2 class="t-accent">About Us</h2> </div>
                <p>Jay Bhavani Steel is a leading manufacturer of Stainless Steel products like Stainless Steel in Bars, Stainless Steel Rounds, Stainless Steel Flats, Stainless Steel Pods, Stainless Steel Coils, Stainless Steel Snapshot, Stainless Steel Pipes, Tubes, Fittings along with some of the other products made from ferrous and non-ferrous metals.</p>
                <p>Our vision serves as the framework for our roadmap and guides every aspect of our business. Our winning culture defines the attitudes and behaviours that will be required for us to make our vision a reality.</p>
                <div class="title m-top-xlg">Strength</div>
                <ul class="m-top-md">
                    <li>Manufacturing Unit comprising separate departments for manufacturing and procurement.</li>
                    <li>Ability to offer products according to the diverse specification of clients in different industries.</li>
                    <li>Developing and maintaining the service and quality of all our products.</li>
                    <li>Vast manufacturing Unit which facilitates the manufacturing of products in bulk in case of emergency.</li>
                    <li>Adherence to the strict Quality Testing and Quality Control parameter.</li>
                </ul>
                <div class="row p-top-xlg">
                    <div class="col-lg-6">
                        <img class="img-responsive m-bottom-lg" src="{{url('front/images/about-us.jpg')}}" alt="About Us">
                    </div>
                    <div class="col-lg-6">
                        <div class="title">Company Objectives</div>
                        <ul>
                            <li>To develop and maintain constant growth and sustain the company’s reputation by sufficing the diverse requirements of the customers.</li>
                            <li>Tracking the scrupulous path made by self made achievements and making improvisation on frequent intervals</li>
                            <li>Developing and maintaining the service and quality of all our products.</li>
                            <li>Building new markets by gaining and retaining, current clients is our sole focus. We perform this by keeping the quality of our products ranked on the top on the quality control and quality testing parameter chart.</li>
                            <li>Research and development helps in identifying new market trends and categorize the application of our products in new markets.</li>
                        </ul>
                    </div>  
                </div>
            </div>
        </section>
    </main>
@endsection