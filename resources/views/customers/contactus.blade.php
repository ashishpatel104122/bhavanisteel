@extends('customers.layout.default')
@section('title','Contact Us | Jay Bhavani Steel')
@section('content')
<main class="innerPage" id="contactus-main">
        <div class="container">
            <section class="section">
                <div class="heading">
                    <h2 class="t-accent">Contact Us</h2>
                </div>
                @php
                    $phones = explode(',',$contactus->phone_no);
                @endphp
                <section class="quickContact">
                    <div class="container">
                        <ul class="row">
                            <li class="col-lg-4">
                                <i class="iconHolder icon-call"></i>
                                @foreach($phones as $phone)
                                <p><a href="tel:{{$phone}}">{{$phone}}</a></p>
                                @endforeach
                            </li>
                            <li class="col-lg-4">
                                <i class="iconHolder icon-mail"></i>
                                <p><a href="mailto:{{ $contactus->email }}">{{ $contactus->email }}</a></p>
                            </li>
                            <li class="col-lg-4">
                                <i class="iconHolder icon-location"></i>
                                <p>{{ $contactus->address }}</p>
                            </li>
                        </ul>
                    </div>
                </section> 
                @if(session()->has('message'))
                <label style="color: green; text-align: center;">{{ session()->get('message') }}</label>
                @endif
                @if(session()->has('error'))
                <label style="color: red; text-align: center;">{{ session()->get('error') }}</label>
                @endif
                <div class="contact-form">
                    <div class="row">
                        <div class="col-lg-3">
                                
                        </div>
                        <div class="col-lg-6">
                            <div class="card noRadius leftPart">
                                <div class="title">Get in touch</div>
                                <form action="{{route('customer.contactus_save')}}" id="" method="post">
                                    @csrf
                                    <div class="fieldControl m-bottom-lg">
                                        <input type="text" class="fieldInput " name="name" maxlength="255" placeholder=" " required="required" value="@if(old('name')!=''){{old('name')}} @endif">
                                        <label class="fieldLabel" for="name">Name*</label>
                                        <i class="fieldIcon icon-user"></i>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                    <div class="fieldControl m-bottom-lg">
                                        <input type="email" class="fieldInput" maxlength="50"  name="email" placeholder=" " required="required" value="@if(old('email')!=''){{old('email')}} @endif">
                                        <label class="fieldLabel" for="name">Email Address*</label>
                                        <i class="fieldIcon icon-mail"></i>
                                        @if($errors->has('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                    <div class="fieldControl m-bottom-lg">
                                        <input type="text" class="fieldInput numeric" min="6" maxlength="15" name="phone" placeholder=" " required="required" value="@if(old('phone')!=''){{old('phone')}} @endif">
                                        <label class="fieldLabel" for="name">Mobile*</label>
                                        <i class="fieldIcon icon-call"></i>
                                        @if($errors->has('phone'))
                                            <span class="text-danger">{{$errors->first('phone')}}</span>
                                        @endif
                                    </div>
                                    <div class="fieldControl m-bottom-lg">
                                        <textarea type="text" name="message" id="message" class="fieldInput fieldTextarea" placeholder=" " required="required" maxlength="500">@if(old('message')!=''){{old('message')}} @endif</textarea>
                                        <label class="fieldLabel" for="message">Message*</label>
                                        <i class="fieldIcon icon-comment"></i>
                                        @if($errors->has('message'))
                                            <span class="text-danger">{{$errors->first('message')}}</span>
                                        @endif
                                    </div>
                                    <div class="t-center"><button class="btn btn-accent">Send Message</button></div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-3">
                                
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
<style type="text/css">
    .text-danger{
        color: #b80924;
    }
</style> 
@push('js')
<script type="text/javascript">
    $('.numeric').on('input', function (event) {
        console.log('dd');
        this.value = this.value.replace(/[^0-9\+.]/g, '');
    });
</script>
@endpush
