<!-- header start -->
<header class="header @if(Route::currentRouteName()=='customer.home') headerHome @endif"  id="header-main">
    <div class="showWap" id="headerWap">
        <div id="hamburgerMenu"><span style="background: #b80924"></span></div>
        <div class="t-left logo">
            <a class="d-inline-block m-left-lg" href="{{route('customer.home')}}">
                @if(Route::currentRouteName()=='customer.home')
                <img src="{{url('front/images/brand-logo-white.png')}}" alt="Jay Bhavani Steel" id="header_logo">
                @else
                <img src="{{url('front/images/brand-logo.png')}}" alt="Jay Bhavani Steel" class="d-inline-block">
                @endif
            </a>
        </div>
        <!-- Side Menu -->
        <nav id="navBody">
            <div id="mySidenav">
                <a href="{{route('customer.home')}}" class="d-inline-block logoNav">
                    <img src="{{url('front/images/brand-logo.png')}}" alt="Jay Bhavani Steel" class="d-inline-block">
                </a>
                <ul class="nav-list">
                    <li><a href="{{route('customer.home')}}"><i class="iconHolder icon-home"></i>Home</a></li>
                    <li>
                        <a href="{{route('customer.category_listing')}}"><i class="iconHolder icon-directory"></i>Our Products</a>
                    </li>
                    <li><a href="{{route('customer.gallery')}}"><i class="iconHolder icon-gallery"></i>Gallery</a></li>
                    <li><a href="{{ route('customer.aboutus') }}"><i class="iconHolder icon-about"></i>About Us</a></li>
                    <li><a href="{{ route('customer.contactus') }}"><i class="iconHolder icon-store-locator"></i>Contact Us</a></li>
                </ul>
                <span class="icon-close" onclick="closeNav()"></span>
            </div>
        </nav>
    </div>
    <div class="showWeb" id="headerWeb">
        <div class="container">
            <div class="row">
                <div class="col-md-4 brandLogo">
                    <a href="{{route('customer.home')}}">
                        <img src="{{url('front/images/brand-logo.png')}}" alt="Jay Bhavani Steel" class="logoAccent">
                        @if(Route::currentRouteName()=='customer.home')
                        <img class="logoWhite" src="{{url('front/images/brand-logo-white.png')}}" alt="Jay Bhavani Steel">
                        @endif
                    </a>
                </div>
                <div class="col-md-8">
                    <nav>
                        <ul>
                            <li><a href="{{route('customer.home')}}" @if(Route::currentRouteName()=='customer.home') style="color: #b80924;" @endif >Home</a></li>
                            <li><a href="{{route('customer.category_listing')}}" @if(Route::currentRouteName()=='customer.category_listing') style="color: #b80924;" @endif>Our Products</a></li>
                            <li><a href="{{route('customer.gallery')}}" @if(Route::currentRouteName()=='customer.gallery') style="color: #b80924;" @endif>Gallery</a></li>
                            <li><a href="{{route('customer.aboutus')}}"  @if(Route::currentRouteName()=='customer.aboutus') style="color: #b80924;" @endif>About</a></li>
                            <li><a href="{{ route('customer.contactus') }}" @if(Route::currentRouteName()=='customer.contactus') style="color: #b80924;" @endif>Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

<style type="text/css">
    .hamburgerMenu span::after, #hamburgerMenu span::before{
        background: #b80924;
    }
    #hamburgerMenu span::after, #hamburgerMenu span::before{
        background: #b80924;
    }
</style>
@push('js')
<script type="text/javascript">
    headerElement=document.getElementById("header-main"),scrollUp="scroll-up",scrollDown="scroll-down";
    window.addEventListener("scroll",function(){ 
        console.log(headerElement.classList[2]);
        if (headerElement.classList[2]=="scroll-up") {
            $("#header_logo").attr('src','front/images/brand-logo.png');
            $('#hamburgerMenu span::after, #hamburgerMenu span::before').css('background','black');
            
            
        }else if (typeof headerElement.classList[2] === "undefined"){
            $("#header_logo").attr('src','front/images/brand-logo-white.png');
        }
    });
</script>
@endpush