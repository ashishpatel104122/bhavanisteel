<!-- footer -->
@php
    $categories = DB::table('categories')->where('parent_id',0)->get();
@endphp
<footer class="footer-wap showWap">
    <div class="container">
        <div class="t-center m-bottom-xlg">
            <img class="footerLogo" src="{{url('front/images/brand-logo-big.png')}}" alt="brand Logo">
        </div>
        
        <div class="links">
            <h3>Quick links</h3>
            <ul>
                <li><a href="{{route('customer.aboutus')}}" style="color: #bdb9bf">About Us</a></li>
                <li><a href="{{route('customer.gallery')}}" style="color: #bdb9bf">Gallery</a></li>
                <li><a href="{{route('customer.contactus')}}" style="color: #bdb9bf">Contact Us</a></li>
            </ul>
        </div>
        <div class="products">
            <h3>Our Products</h3>
            <ul>
                @foreach($categories as $category)
                     <li><a href="{{url('category-listing?id='.$category->id)}}"  style="color: #bdb9bf">{{$category->name}}</a></li>
                @endforeach
            </ul>
        </div>
        <!-- <div class="footerSocial m-top-xlg m-bottom-lg">
            <h3>Follow Us</h3>
            <div class="child-d-inline-block m-top-md">
                <a href="" target="_blank" rel="noopener" class="icon-facebook"></a>
                <a href="" target="_blank" rel="noopener" class="icon-youtube"></a>
                <a href="" target="_blank" rel="noopener" class="icon-twitter"></a>
                <a href="" target="_blank" rel="noopener" class="icon-instagram"></a>
            </div>
        </div> -->
    </div>
    <div class="copyright p-md">
        <div class="text text-sm">
            © Copyright 2020. All Rights Reserved.
        </div>
    </div>
</footer>
<footer class="footer-web showWeb">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-2">
                <img class="footerLogo" src="{{url('front/images/brand-logo-big.png')}}" alt="brand Logo">
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-2">
                <h3>Quick Link</h3>
                <ul class="m-top-md footerLink">
                    <li><a href="{{route('customer.aboutus')}}">About Us</a></li>
                    <li><a href="{{route('customer.gallery')}}">Gallery</a></li>
                    <li><a href="{{route('customer.contactus')}}">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <h3>Our Products</h3>
                <ul class="m-top-md footerLink">
                    @foreach($categories as $category)
                        <li><a href="{{url('category-listing?id='.$category->id)}}">{{$category->name}}</a></li>
                    @endforeach
                </ul>
            </div>
            <!-- <div class="col-lg-2">
                <h3>Follow Us</h3>
                <div class="child-d-inline-block footerSocial m-top-md">
                    <a href="" target="_blank" rel="noopener" class="icon-facebook"></a>
                    <a href="" target="_blank" rel="noopener" class="icon-youtube"></a>
                    <a href="" target="_blank" rel="noopener" class="icon-twitter"></a>
                    <a href="" target="_blank" rel="noopener" class="icon-instagram"></a>
                </div>
            </div> -->
        </div>
    </div>
    <div class="copyright p-lg">
        <div class="t-center">
            © Copyright 2020. All Rights Reserved.</a>
        </div>
    </div>
</footer>