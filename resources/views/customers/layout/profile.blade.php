<style>
    #profileInfo{
        padding: 0 40px;
        margin: 0;
        width: 70%;
        position: absolute;
        top: 50%;
        left: 60%;
        transform: translate(-50%, -50%);
    }
</style>
<div class="modal fade login-signup" id="Profile" role="dialog">
    <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            @if(auth()->check())
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <section id="formHolder">
                        <div class="row">
                           <!-- Brand Box -->
                           <div class="col-sm-6 brand">
                              <div class="heading">
                                  <img src="{{ asset('customers/images/main-logo.svg') }}" alt="">
                              </div>
                  
                              <div class="success-msg">
                                 <p>Great! You are one of our members now</p>
                                 <a href="#" class="profile">Your Profile</a>
                              </div>
                           </div>
                  
                  
                           <!-- Form Box -->
                           <div class="col-sm-6 form">
                              <!-- Login Form -->
                              <div class="login form-peice switched">
                                 <form class="" id="profile" action="customer/profile" method="post">
                                 {{ csrf_field() }}
                                    <div class="form-group">
                                       <label for="name">Name</label>
                                       <input type="name" name="name" id="profileID" class="name" value="{{ Auth::user()->name ? Auth::user()->name : '' }}" required>
                                    </div>

                                    <div class="form-group">
                                       <label for="email">Email</label>
                                       <input type="email" name="email" id="profile_email" class="email" value="{{ Auth::user()->email ? Auth::user()->email : '' }}" required>
                                    </div>

                                    <div class="form-group">
                                       <label for="stripe_id">Stripe ID</label>
                                       <input type="number" name="stripe_id" id="profile_stripeID" class="stripe_id" value="{{ Auth::user()->stripe_id ? Auth::user()->stripe_id : '' }}" >
                                    </div>

                                    <div class="form-group">
                                       <label for="card_brand">Card Brand</label>
                                       <input type="text" name="card_brand" id="profile_cardBank" class="card_brand" value="{{ Auth::user()->card_brand ? Auth::user()->card_brand : '' }}">
                                    </div>
                  
                                    <div class="CTA">
                                       <input type="submit" value="Submit">
                                       <a href="#" class="switch">View</a>
                                    </div>
                                 </form>
                              </div><!-- End Login Form -->
                              <!-- Signup Form -->
                              <div class="signup form-peice">
                                  <table class="table" id="profileInfo">
                                      <tr>
                                          <td><label>Name</label></td>
                                          <td><label>{{ Auth::user()->name ? Auth::user()->name : "-" }}</label></td>
                                      </tr>
                                      <tr>
                                          <td><label>Email</label></td>
                                          <td><label>{{ Auth::user()->email ? Auth::user()->email : "-" }}</label></td>
                                      </tr>
                                      <!-- <tr>
                                          <td><label>Stripe ID</label></td>
                                          <td><label>{{ Auth::user()->stripe_id ? Auth::user()->stripe_id : "-" }}</label></td>
                                      </tr>
                                      <tr>
                                          <td><label>Card Brand</label></td>
                                          <td><label>{{ Auth::user()->card_brand ? Auth::user()->card_brand : "-" }}</label></td>
                                      </tr> -->
                                      <tr>
                                          <td colspan="2" style="text-align: center;" class="CTA"><a href="#" class="CTA switch">Edit Profile</a></td>
                                      </tr>
                                  </table>
                              </div><!-- End Signup Form -->
                           </div>
                        </div>
                  
                     </section>    
                </div>
            </div>
            @endif
    </div>
  </div>