<ul class="mobile-sub wsmenu-list">
    <li>
        <a href="{{url('/')}}">
            Home
        </a>
    </li>
    <li>
        <a href="#">
            Category
            <span class="arrow">
            </span>
        </a>
        <div class="megamenu clearfix w-auto-menu-wrap">
            <ul class="col-sm-12 link-list">
                @php
                    $cartegorys = App\Shop\Categories\Category::get();
                @endphp
                <li>
                    @foreach($cartegorys as $category)
                    <a href="{{url('customer/products',$category->id)}}" class="hover_color">
                        <i class="fa fa-tag">
                        </i>
                        {{$category->name}}
                    </a>
                    @endforeach
                </li>
            </ul>
        </div>
    </li>
    <li>
        <a href="#">
            Catalogues
        </a>
    </li>
    <li>
        <a href="{{route('customer.news')}}">
            News
        </a>
    </li>
    <li>
        <a href="#">
            Career
        </a>
    </li>
    <li>
        <a href="#">
            About Us
        </a>
    </li>
    <li>
        <a href="{{route('customer.contactus')}}">
            Contact Us
        </a>
    </li>
</ul>
<div class="social-header"> 
    <a href="https://www.facebook.com/Hawk-Granito-685448531819854/" target="_blank">
        <i class="fa fa-facebook"></i>
    </a>
    <a href="https://www.linkedin.com/in/hawk-granito-11146616b/" target="_blank">
        <i class="fa fa-linkedin"></i>
    </a>
    <a href="https://www.instagram.com/hawkgranito/" target="_blank">
        <i class="fa fa-instagram"></i>
    </a>
    <a href="https://www.pinterest.com/hawkgranito/" target="_blank">
        <i class="fa fa-pinterest"></i>
    </a>
</div>        
<style type="text/css">
    .hover_color:hover{
        color:#a98970!important;
    }
</style>