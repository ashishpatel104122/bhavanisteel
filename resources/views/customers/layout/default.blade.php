<!doctype html>
<html lang="en">
  <head>
       <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-XXXXX-Y', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
    <!-- Google Tag Manager -->
    <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PX6D2DW');
    </script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name=format-detection content="telephone=no">
    <meta name="google-site-verification" content="3L0nOE2Vu2r1Hvpe6ZJrIO4HFH49JBrkrTLqdnZ_89A" />
    <meta name=viewport content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href=https://jaybhavanisteel.com/assets/images/logo.png type=image/x-icon>
    
    <link rel=canonical href=https://www.jaybhavanisteel.com/ >
    <meta property=og:locale content=en_US>
    <meta property=og:type content=website>
    <meta property=og:title content="Stainless Steel Products | Fittings | Aluminium Products | Jay Bhavani Steel">
    <meta property=og:description content="Manufacturer of stainless steel products as SS bars, rounds, flats, pods, coils, snapshot, pipes, tubes fittings, sheets, plates in hexagonal, square for chemical, fertilizers, oil processing, textile machinery, synthetic fibers, power plants, dairy by Jay Bhavani Steel Company in India.">
    <meta property=og:url content=https://www.jaybhavanisteel.com/ >
    <meta property=og:site_name content="Jay Bhavani Steel">
    <meta name=twitter:card content=summary_large_image>
    <meta name=twitter:description content="Manufacturer of stainless steel products as SS bars, rounds, flats, pods, coils, snapshot, pipes, tubes fittings, sheets, plates in hexagonal, square for chemical, fertilizers, oil processing, textile machinery, synthetic fibers, power plants, dairy by Jay Bhavani Steel in India.">
    <meta name=twitter:title content="Stainless Steel Products | Fittings | Aluminium Products | Jay Bhavani Steel">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>
    <meta name="description" content="Manufacturer of stainless steel products as SS bars, rounds, flats, pods, coils, snapshot, pipes, tubes fittings, sheets, plates in hexagonal, square for chemical, fertilizers, oil processing, textile machinery, synthetic fibers, power plants, dairy by Jay Bhavani Steel in India">
	<meta name="keywords" content="Stainless Steel Products | Fittings | Aluminium Products | Jay Bhavani Steel"/>
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Bootstrap -->
    <!-- Add Css here -->
    <link rel="stylesheet" type="text/css" href="{{url('front/public/build/css/bundle1.css')}}">
    <style type="text/css">
        .floating-wpp-button{
            width: 50px!important;
            height: 50px!important;
        }
    </style>
   
    @stack('css')
  </head>
  <body>
    <!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PX6D2DW" height="0" width="0" style="display:none;visibility:hidden">
    </iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->  
    <div class="row" style="margin-left: 317px; margin-right: 362px;">
        <div class="col-md-12">
            @if(session()->has('error'))
            <div class="pt0 flashmsg">
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert">X</a>
                    <strong>{{ session('error') }}</strong>
                </div>
            </div>
            <?php Session::forget('error') ?>
            @endif
            @if(session()->has('success'))
            <div class="pt0 flashmsg">
                <div class="alert alert-success">
                    <a class="close" data-dismiss="alert">X</a>
                    <strong>{{ session('success') }}</strong>
                </div>
            </div>
            <?php Session::forget('success') ?>
            @endif
        </div>
  </div>
  <div class="main-div">
  @include('customers.layout.header')
      <div id="WAButton"></div>
    @yield('content')
  @include('customers.layout.footer')
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Add Js here -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <!-- GetButton.io widget -->
    <!--Jquery-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <!--Floating WhatsApp css-->
    <link rel="stylesheet" href="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.css">
    <!--Floating WhatsApp javascript-->
    <script type="text/javascript" src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.js"></script>
    <!-- JS Bundle -->
    <script src="{{url('front/public/build/js/bundle-min1.js')}}"></script>
    <script type="text/javascript">
        $(function() {
            function isMobile() 
            {
                return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
            }
            $('#WAButton').floatingWhatsApp({
                phone: '+917574048139', //WhatsApp Business phone number International format-
                headerTitle: 'Chat with us on WhatsApp!', //Popup Title
                popupMessage: 'Hello, how can we help you?', //Popup Message
                showPopup: true, //Enables popup display
                autoOpen: true, // true or false
                buttonImage: '<img src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/whatsapp.svg" />', //Button Image
                // headerColor: 'crimson', //Custom header color
                backgroundColor: 'crimson', //Custom background button color
                position: "right"    
            });
        });
    </script>
    @stack('js')

</body>
</html>
