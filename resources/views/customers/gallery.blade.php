@extends('customers.layout.default')
@section('title','Gallery | Jay Bhavani Steel')
@section('content')
    <main class="innerPage" id="gallery-main">
        <section class="section">
            <div class="heading t-center">
                <h2 class="t-accent">Our Gallery</h2>
                <p class="p-xy-sm m-bottom-lg">we have the right products to fit your needs and budget</p>
            </div>
            <div class="our-projects">
                <ul class="tabs swipeSlider child-d-inline-block t-center">
                    <li class="tab-link active" data-tab="tab-1">All</li>
                    @foreach($categories as $category)
                    <li class="tab-link" data-tab="tab-{{$category->id}}">{{$category->name}}</li>
                    @endforeach
                </ul>
                <div class="container">
                    <div class="tab-content-wrap">
                        <div class="tab-content current" id="tab-1">
                            @foreach($products as $products)
                                @if(count($products->product_image)!=0)
                                    @foreach($products->product_image as $products)
                                        <a href="#"><img src="{{url($products->image)}}" alt="Our Project Img" style="width: 250px; height: 250px"></a>
                                    @endforeach
                                @endif
                            @endforeach
                        </div>
                        @foreach($categories as $category)
                                <div class="tab-content" id="tab-{{$category->id}}">
                                    @php
                                    $category_product = DB::table('categories')->select('categories.id as id','products.id as product_id','product_images.image')
                                        ->leftjoin('products','products.category_id','categories.id')
                                        ->leftjoin('product_images','product_images.product_id','products.id')
                                        ->where('categories.id',$category->id)
                                        ->where('products.status',1)
                                        ->get();
                                    @endphp    
                                    @foreach($category_product as $category)
                                    @if($category->image)
                                    <a href="#"><img src="{{url($category->image)}}" alt="Our Project Img" style="width: 250px; height: 250px"></a>
                                    @endif
                                    @endforeach
                                </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection    