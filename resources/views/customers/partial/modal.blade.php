<div id="modalSmall" class="productModal modalBody hide modalHide">
    <div class="modalMain">
        <div class="modalContent thinScrollBar scrollY pos-r" style="max-height:80vh">
            <div class="container">
                <div class="cardName">{{$product_array->name}}</div>
                <div class="row">
                    <div class="col-lg-12">
                        @if($product_array->product_image)
                        <div class="slideshow-container">
                            @foreach($product_array->product_image as $key=> $image)
                            <div class="mySlides fade">
                              <div class="numbertext">{{$key+1}} / {{count($product_array->product_image)}}</div>
                              <img src="{{url($image->image)}}" style="max-width: 100%;max-height: 100%;display: block;margin: auto;">
                            </div>
                            @endforeach
                            <a class="prev" onclick="plusSlides(-1)" style="background-color:black;">&#10094;</a>
                            <a class="next" onclick="plusSlides(1)" style="background-color:black;">&#10095;</a>
                        </div>
                        <br>
                        <div style="text-align:center">
                            @foreach($product_array->product_image as $key=> $image)
                                <span class="dot" onclick="currentSlide('{{$key+1}}')"></span> 
                            @endforeach
                        </div>
                        @endif
                    </div>
                    <div class="col-lg-12">
                        <div class="detailsWrap">
                            <p>{!! $product_array->description !!}</p>
                        </div>
                        <table class="table table-bordered tableWrap" style="margin-bottom: 5px">
                            <tbody>
                                @if($product_array->key!="" && $product_array->value!="")
                                    @php
                                        $keys = explode(',',$product_array->key);
                                        $values = explode(',',$product_array->value);
                                    @endphp
                                    @foreach($keys as $key => $value)
                                    <tr>
                                        <td>{{$value}}</td>
                                        <td>{{$values[$key]}}</td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <span class="closeIconBtn" onclick="modalHidden('modalSmall')" title="Close"></span>
        <a class="quoteBtn" href="{{route('customer.contactus')}}"><i class="icon-mail"></i>Get A Quote</a>
    </div>
</div>
<style type="text/css">
.detailsWrap, .column_column , .column_attr  > ul > li{
  list-style:inside;
} 
table{
    width: 100%!important;
    height: 100%!important;
    display: block;
    overflow: auto;
}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}


/* Number text (1/3 etc) */
.numbertext {
  color: black;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 8px;
  width: 8px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>
<script type="text/javascript">
    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) { slideIndex = 1 }
        if (n < 1) { slideIndex = slides.length }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        if (slides[slideIndex - 1]) {
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
        }
    }
    $(document).keyup(function(e) {
      if (e.key === "Escape") {
        $(".modalBody").addClass("hide modalHide");
      }
    });
</script>