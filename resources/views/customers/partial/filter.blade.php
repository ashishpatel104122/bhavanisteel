<div class="container">
    <div class="t-right">
        <span class="filterBtn icon-filter showWap"  onclick="openFilter()" style="color: #b80924;position: relative;bottom: 24px;font-size: 20px;" id="filterBtn"></span>
    </div>
    <div class="row" id="cat_listing">
        @foreach($category_array as $category)
            @foreach($category as $category)
            <div class="col-lg-4">
                <div class="pro-card" onclick="modalToggle('modalSmall','{{$category->id}}')" data-dismiss="modal">
                    <div class="imgHolder">
                       @if($category->background_image) 
                        <img class="img-responsive" src="{{url($category->background_image)}}" alt="Product 1">
                       @endif
                    </div>
                    <div class="proName">{{$category->name}}</div>
                </div>
            </div>
            @endforeach
        @endforeach
    </div>
</div>


