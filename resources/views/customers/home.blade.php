@extends('customers.layout.default')
@section('title','Stainless Steel Products | Fittings | Aluminium Products | Jay Bhavani Steel')
@section('content')
<!-- Home Page -->
    <main id="home">
        <!-- Banner Slider Start -->
        <div class="heroBanner">
            <ul class="mobileBanners showWap">
                <li>
                    <div class="imgBanner" style="background: url('/front/images/banners/desktop/banner-2.jpg');"></div>
                </li>
                <li>
                    <div class="imgBanner" style="background: url('/front/images/Steel-Industry.jpg');"></div>
                </li>
            </ul>
            <ul class="webBanners showWeb">
                <li>
                    <div class="imgBanner" style="background: url('/front/images/banners/desktop/banner-2.jpg');"></div>
                </li>
                <li>
                    <div class="imgBanner" style="background: url('/front/images/Steel-Industry.jpg');"></div>
                </li>
            </ul>
            <div class="centerText">
                <h1>BHAVANI STEEL , COST EFFECTIVE LONG LASING STEELS.</h1>
                <!-- <p>BHAVANI STEEL , Cost effective long lasing steels!</p> -->
                <a class="btn btn-accent" href="{{route('customer.aboutus')}}">Learn More</a>
                <a class="btn btnAccentBorder" href="{{route('customer.contactus')}}">Contact us</a>
            </div>
        </div>
        <!-- Banner Slider End -->

        <!-- Welcome to bhavani section start -->
        <section class="section welcomeSec">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 left-side">
                        <h2>Welcome to Bhavani Steel</h2>
                        <p class="first">Jay Bhavani Steel is a leading manufacturer of Stainless Steel products like Stainless Steel in Bars, Stainless Steel Rounds, Stainless Steel Flats, Stainless Steel Pods, Stainless Steel Coils, Stainless Steel Snapshot, Stainless Steel Pipes, Tubes, Fittings along with some of the other products made from ferrous and non-ferrous metals.</p>
                        <a href="{{route('customer.aboutus')}}" class="btn btnAccentBorder m-top-lg">LEARN MORE</a>
                    </div>
                    <div class="col-lg-6 right-side">
                        <div class="imgHolder">
                            <img src="{{url('front/images/welcome.jpg')}}" alt="Welcome TO Bhavani">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Our Product -->
        <section class="section t-center ourProduct bgGray">
            <div class="heading"><h2>OUR PRODUCTS</h2></div>
            <div class="container">
                <div class="row">
                    @foreach($products as $product)
                    <div class="col-lg-4">
                        <div class="pro-card" onclick="modalToggle('modalSmall','{{$product->id}}')" data-dismiss="modal">
                            <div class="imgHolder">
                                <img class="img-responsive" src="{{url($product->background_image)}}" alt="Product 1">
                            </div>
                            <div class="proName">{{$product->name}}</div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <a class="btn-link" href="{{url('/category-listing')}}">View All Products</a>
            </div>
        </section>

        <!-- Our Services -->
        <!-- <section class="section ourServices">
            <div class="container">
                <div class="heading"><h2>Our Services</h2></div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="servicesCard">
                            <div class="imgHolder">
                                <img class="img-responsive" src="{{url('front/images/our-services-img-1.jpg')}}" alt="Steel Sourcing">
                            </div>
                            <div class="details">
                                <h3>Steel Sourcing</h3>
                                <p class="text m-top-md">We regularly audit and monitor all of our steel supply sources for quality and performance to ensure you receive the best materials.</p>
                                <div class="btn btn-accent m-top-lg">Read More</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="servicesCard">
                            <div class="imgHolder">
                                <img class="img-responsive" src="{{url('front/images/our-services-img-4.jpg')}}" alt="Steel Sourcing">
                            </div>
                            <div class="details">
                                <h3>Material Processing</h3>
                                <p class="text m-top-md">We have the capacity to process raw material into finished goods in our workshop that has a range of modern technology machinery</p>
                                <div class="btn btn-accent m-top-lg">Read More</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="servicesCard">
                            <div class="imgHolder">
                                <img class="img-responsive" src="{{url('front/images/our-services-img-3.jpg')}}" alt="Steel Sourcing">
                            </div>
                            <div class="details">
                                <h3>Steel Welding</h3>
                                <p class="text m-top-md">We have the capacity to weld mild steel, stainless steel and aluminium as well as advise the most effective method of joining metals.</p>
                                <div class="btn btn-accent m-top-lg">Read More</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- Industries We Served -->
        <section class="section industriesServed">
            <div class="parallax section">
                <div class="heading"><h2>Industries We Served</h2></div>
                <div class="container">
                    <ul class="row">
                        <li class="col-lg-4">
                            <div class="industriesCard">
                                <h4 class="text-industries">Heavy Fabrication</h4>
                            </div>
                        </li>
                        <li class="col-lg-4">
                            <div class="industriesCard">
                                <h4 class="text-industries">Engineering</h4>
                            </div>
                        </li>
                        <li class="col-lg-4">
                            <div class="industriesCard">
                                <h4 class="text-industries">Pharma</h4>
                            </div>
                        </li>
                        <li class="col-lg-4">
                            <div class="industriesCard">
                                <h4 class="text-industries">Food</h4>
                            </div>
                        </li>
                        <li class="col-lg-4">
                            <div class="industriesCard">
                                <h4 class="text-industries">Petrochemicals</h4>
                            </div>
                        </li>
                        <li class="col-lg-4">
                            <div class="industriesCard">
                                <h4 class="text-industries">Chemical</h4>
                            </div>
                        </li>
                        <li class="col-lg-4">
                            <div class="industriesCard">
                                <h4 class="text-industries">Fertilizer</h4>
                            </div>
                        </li>
                        <li class="col-lg-4">
                            <div class="industriesCard">
                                <h4 class="text-industries">Oil Processing</h4>
                            </div>
                        </li>
                        <li class="col-lg-4">
                            <div class="industriesCard">
                                <h4 class="text-industries">Synthetic Fibers</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        
        <!-- Quick Contact -->
        @php
            $phones = explode(',',$contactus->phone_no);
        @endphp
        <section class="quickContact">
            <div class="container">
                <ul class="row">
                    <li class="col-lg-4">
                        <i class="iconHolder icon-call"></i>
                        @foreach($phones as $phone)
                        <p><a href="tel:{{$phone}}">{{$phone}}</a></p>
                        @endforeach
                    </li>
                    <li class="col-lg-4">
                        <i class="iconHolder icon-mail"></i>
                        <p><a href="mailto:{{ $contactus->email }}">{{ $contactus->email }}</a></p>
                    </li>
                    <li class="col-lg-4">
                        <i class="iconHolder icon-location"></i>
                        <p>{{ $contactus->address }}</p>
                    </li>
                </ul>
            </div>
        </section>
        <div id="modalData"></div> 
    </main>
@endsection  
@push('css')
<style type="text/css">
    .text-industries{
        padding: inherit;
        pointer-events: initial;
        cursor: pointer;
    }
</style>
@endpush
@push('js')
<script>
    function modalToggle(id, pro_id){
        $.ajax({
            url:"/get-product-model",
            type:"POST",
            data:{
                '_token':"{{csrf_token()}}",
                'id':id,
                'pro_id':pro_id,
            },             
            dataType:"text",
            success:function(data) {

                $("#modalData").html(data);
                $("table").addClass("table table-bordered tableWrap");
                document.getElementById(id).classList.toggle("hide");
                document.getElementById(id).classList.toggle("modalHide");
                if(document.getElementById(id).classList.contains('modalHide')){
                    document.body.style.overflow = "auto";
                }else{
                    document.body.style.overflow = "hidden";
                }
            }
        });
    }
    function modalHidden(id){
        document.getElementById(id).classList.toggle("hide");
        document.getElementById(id).classList.toggle("modalHide");
        if(document.getElementById(id).classList.contains('modalHide')){
            document.body.style.overflow = "auto";
        }else{
            document.body.style.overflow = "hidden";
        }
    }
</script>
@endpush