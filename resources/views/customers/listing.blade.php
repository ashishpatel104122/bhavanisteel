@extends('customers.layout.default')
@if($category_name)
@section('title',"$category_name->name".' | Jay Bhavani Steel')
@else
@section('title','Stainless Steel Products | Fittings | Aluminium Products | Jay Bhavani Steel')
@endif
@section('content')
<main class="innerPage" id="listing-main">
        <!-- <div class="topFilter">
            <div class="container">
                <h2>Stainless Steel Flats</h2>
            </div>
        </div> -->
       <div class="listing-wrap pos-r clearfix">
            @if($is_mobile)
            <aside class="t-left filter-panel pos-r" id="filter-panel">
            @else
            <aside class="t-left filter-panel pos-r" id="web-filter-panel">
            @endif
            <div class="filterCont">
                <span class="icon-close showWap" onclick="closeFilter()"></span>
                <div class="filter-header d-table-main">
                    <h2 class="fitlter-title">Filters</h2>
                    <span class="clearBtn t-right cursor-pointer" onclick="window.location.reload(true);">Refresh</span>
                </div>
                <ul class="filter-main">
                    <li class="filter-group">
            
                        <div class="filter-head">
                            <i class="icon-directory"></i>
                            <span>Categories</span> 
                        </div>
                        <div class="filter-content">
                            @foreach($categorys as $category)
                            <label class="customControl customCheckbox categoryLabel">
                                <input name="" type="checkbox" class="category_select" required="" value="{{$category->id}}" @if($category_id && $category_id==$category->id) checked="" @endif> 
                                <span class="itemName">{{$category->name}}</span>
                                <div class="customIndicator"></div> 
                            </label>
                            @endforeach
                        </div>
                    </li>
                    <!-- Price filter -->
                    <!-- <li class="filter-group">
                        <div class="filter-head">
                            <i class="icon-directory"></i>
                            <span>PRICE</span> 
                        </div>
                        <div class="filter-content">
                            <label class="customControl customCheckbox priceFil">
                                <input name="" type="checkbox" class="pricelist" required="" value="5000-10000"> 
                                <span class="itemName"><i class="icon-rupee"></i>5,000 - <i class="icon-rupee"></i>10,000</span>
                                <div class="customIndicator"></div> 
                            </label>
                            <label class="customControl customCheckbox priceFil">
                                <input name="" type="checkbox" required="" class="pricelist" value="10000-15000"> 
                                <span class="itemName"><i class="icon-rupee"></i>10,000 - <i class="icon-rupee"></i>15,000</span>
                                <div class="customIndicator"></div> 
                            </label>
                            <label class="customControl customCheckbox priceFil">
                                <input name="" type="checkbox" required="" class="pricelist" value="15000-20000"> 
                                <span class="itemName"><i class="icon-rupee"></i>15,000 - <i class="icon-rupee"></i>20,000</span>
                                <div class="customIndicator"></div> 
                            </label>
                            <label class="customControl customCheckbox priceFil">
                                <input name="" type="checkbox" required="" class="pricelist" value="25000"> 
                                <span class="itemName"><i class="icon-rupee"></i>25,000 +</span>
                                <div class="customIndicator"></div> 
                            </label>
                        </div>
                    </li> -->
                </ul>
                <div class="filterFooter t-upper showWap">
                    <a href="javascript:void(0)" onclick="closeFilter()" class="btn btn-primary btnApply w-full">Apply</a>
                </div>
            </div>
           </aside>
            <div class="web-listing-main">
                <div class="container">
                    <div class="t-right">
                        <span class="filterBtn icon-filter showWap"  onclick="openFilter()" style="color: #b80924;position: relative;bottom: 24px;font-size: 20px;" id="filterBtn"></span>
                    </div>
                    <div class="row" id="cat_listing">
                        @foreach($products as $product)
                            <div class="col-lg-4">
                                <div class="pro-card" onclick="modalToggle('modalSmall','{{$product->id}}')" data-dismiss="modal">
                                    <div class="imgHolder">
                                        @if($product->background_image)
                                        <img class="img-responsive" src="{{url($product->background_image)}}" alt="Product 1">
                                        @endif
                                    </div>
                                    <div class="proName">{{$product->name}}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
       </div>
       <!-- Product Popup -->
        <!-- <div id="modalBig" class="productModal modalBody hide modalHide">
            <div class="modalMain">
                <div class="modalContent thinScrollBar scrollY pos-r" style="max-height:80vh">
                    <div class="container">
                        <div class="cardName">Stainless Steel Wire Rods</div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card cardquickproduct shadowNone cardquickproductImage" id="">
                                    <ul class="productListSlider">
                                        <li>
                                            <div class="imgHolder">
                                                <img src="{{url('background_image/f2c9404fa5e60dac9a5c55a446665780.jpeg')}}" alt="Stainless Steel Wire Rods">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="imgHolder">
                                                <img src="{{url('background_image/f2c9404fa5e60dac9a5c55a446665780.jpeg')}}" alt="Stainless Steel Wire Rods">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="detailsWrap">
                                    <p>We stock Stainless Steel, carbon and alloy specialty steels supplied in the as rolled, annealed and normalized conditions. We offer cut to size and heat treatment through our service centre.</p>
                                    <p>We also stock flat, Hexagon, Triangle Bar, Square, Half Round products in various dimensions and grades.We offer a speedy delivery service in order to get your products to you on time.</p>
                                    <h4>User Industries:</h4>
                                    <p>Chemicals, Fats, & Fertilizers, Sugar Mills & Distilleries, Cement Industries, Ship Builders, Paper Industries, Pumps, Petrochemicals, Oil & Natural Gas Organization in terms of the specific materials, Technology, Finance & Personal.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row tableWrap">
                            <div class="col-lg-6">
                                <div class="detailsWrap">
                                    <p>We stock Stainless Steel, carbon and alloy specialty steels supplied in the as rolled, annealed and normalized conditions. We offer cut to size and heat treatment through our service centre.</p>
                                    <p>We also stock flat, Hexagon, Triangle Bar, Square, Half Round products in various dimensions and grades.We offer a speedy delivery service in order to get your products to you on time.</p>
                                    <h4>User Industries:</h4>
                                    <p>Chemicals, Fats, & Fertilizers, Sugar Mills & Distilleries, Cement Industries, Ship Builders, Paper Industries, Pumps, Petrochemicals, Oil & Natural Gas Organization in terms of the specific materials, Technology, Finance & Personal.</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="title">Stainless Steel Cold Heading Wires</div>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr> <td>Condition</td> <td>Cold drawn, Annealed and Pickled</td> </tr>
                                        <tr> <td>Diameter (Size)</td> <td>1.6 mm to 17 mm (1/16" to 11/16")</td> </tr>
                                        <tr> <td>Diameter Tolerance</td> <td>+/- 0.050 mm</td> </tr>
                                        <tr> <td>Tensile Strength</td> <td>65 kg / mm2 max</td> </tr>
                                        <tr> <td>Packing</td> <td>HDPE Wrapped coils of 300 kg to 500 kg</td> </tr>
                                        <tr> <td>Grades</td> <td>304, 304L, 316, 316L, 304HC, 302HQ</td> </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="closeIconBtn" onclick="modalToggle('modalBig')" title="Close"></span>
            </div>
        </div> -->
        
    </main>
    <div id="modalData">
    </div>
@endsection

@push('js')

<script>

    $('.pro-card').click( function(){
        console.log('d');
    });
    $(".category_select").click( function(){
        var category_select = $('.categoryLabel input:checked');
        category = [];
        $.each(category_select, function(key, value){
            category.push(value.value);
        });
        var price_select = $('.priceFil input:checked');
        price = [];
        $.each(price_select, function(key, value){
            price.push(value.value);
        });
        getData(category, price);
    });
    $(".pricelist").click( function(){
        var category_select = $('.categoryLabel input:checked');
        category = [];
        $.each(category_select, function(key, value){
            category.push(value.value);
        });
        var price_select = $('.priceFil input:checked');
        price = [];
        $.each(price_select, function(key, value){
            price.push(value.value);
        });
        getData(category,price);
    });
    function modalToggle(id, pro_id){
        $.ajax({
            url:"/get-product-model",
            type:"POST",
            data:{
                '_token':"{{csrf_token()}}",
                'id':id,
                'pro_id':pro_id,
            },             
            dataType:"text",
            success:function(data) {
                $("#modalData").html(data);
                $("table").addClass("table table-bordered tableWrap");
                document.getElementById(id).classList.toggle("hide");
                document.getElementById(id).classList.toggle("modalHide");
                if(document.getElementById(id).classList.contains('modalHide')){
                    document.body.style.overflow = "auto";
                }else{
                    document.body.style.overflow = "hidden";
                }
            }
        });
    }
    function modalHidden(id){
        document.getElementById(id).classList.toggle("hide");
        document.getElementById(id).classList.toggle("modalHide");
        if(document.getElementById(id).classList.contains('modalHide')){
            document.body.style.overflow = "auto";
        }else{
            document.body.style.overflow = "hidden";
        }
    }

    function getData(id , price)
    {
        $.ajax({
            url:"/get-category-list",
            type:"POST",
            data:{
                '_token':"{{csrf_token()}}",
                'id':id,
                'price':price,
            },             
            dataType:"text",
            success:function(data) {
                $('.web-listing-main').empty();
                $('#modalSmall').empty();
                $('.web-listing-main').html(data);
            },
            error: function(error){
            }
        });
    }
</script>
@endpush