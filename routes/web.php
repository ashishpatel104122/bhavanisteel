<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/** * Admin routes
 */
Route::namespace('Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.' ], function () {
    Route::namespace('Admin')->group(function () {
            Route::post('getcategory', 'Products\ProductController@getCategory')->name('admin.getcategory');
            Route::get('/', 'DashboardController@index')->name('dashboard');
            Route::namespace('Products')->group(function () {
                Route::resource('products', 'ProductController');
                Route::get('remove-image-thumb/{id}', 'ProductController@removeThumbnail')->name('product.remove.thumb');
                Route::get('products-search', 'ProductController@index')->name('products.search');
                Route::get('stock-products', 'ProductController@stocklist')->name('stocklist.products');
                Route::get('editstock/products/{id}', 'ProductController@stockedit')->name('stockedit.products');
                Route::post('updatestock/products', 'ProductController@stockupdate')->name('stockupdate.products');
            });

            Route::namespace('Categories')->group(function () {
                Route::resource('categories', 'CategoryController');
                Route::get('remove-image-category', 'CategoryController@removeImage')->name('category.remove.image');
                Route::get('categories-search', 'CategoryController@index')->name('categories.search');
                Route::get('category-child/{parent_id}', 'CategoryController@childCategory')->name('category.child');
            });
            

            //News
            Route::get('news','News\NewsController@create')->name('news.create');
            Route::post('news-store','News\NewsController@store')->name('news.store');
            Route::get('news-list','News\NewsController@index')->name('news.list');
            Route::get('news/delete/{id}','News\NewsController@destroy')->name('news.destroy'); 
            Route::get('news/edit/{id}','News\NewsController@edit')->name('news.edit');
            Route::get('news-search','News\NewsController@index')->name('news.search');
            Route::post('news/status/{id}', 'News\NewsController@status')->name('news.status');

            
            Route::resource('employees', 'EmployeeController');
            Route::post('employees/is_active/{id}', 'EmployeeController@is_active')->name('employees.is_active');
            Route::get('profile/{id}', 'EmployeeController@getProfile')->name('employee.profile');
            Route::put('profile/{id}', 'EmployeeController@updateProfile')->name('employee.profile.update');
            Route::resource('permissions', 'Permissions\PermissionController');

            //CMS
            Route::get('cms-create','Cms\CmsController@create')->name('cms.create');
            Route::post('cms-store','Cms\CmsController@store')->name('cms.store');
            Route::get('cms','Cms\CmsController@index')->name('cms.list');
            Route::post('cms/delete/{id}','Cms\CmsController@destroy')->name('cms.destroy'); 
            Route::get('cms/edit/{id}','Cms\CmsController@edit')->name('cms.edit');
            Route::post('cms/status/{id}', 'Cms\CmsController@status')->name('news.status');

            //ContactUs
            Route::get('contactus-list','ContactusController@index')->name('contactus.list');
            Route::post('contactus/delete/{id}','ContactusController@destroy')->name('contactus.destroy'); 

    });    
});

/**
 * Frontend routes unused
 */

Route::namespace('Customer')->group(function () {
    Route::get('/', 'HomeController@index')->name('customer.home');
    // Contact Us
    Route::get('/contact-us', 'ContactUsController@index')->name('customer.contactus');
    Route::post('/contact-us/save', 'ContactUsController@store')->name('customer.contactus_save');

    // Gallery
    Route::get('/gallery', 'GalleryController@index')->name('customer.gallery');

    // Category Listing
    Route::get('/category-listing', 'CategoryController@index')->name('customer.category_listing');
    Route::post('/get-category-list', 'CategoryController@getCategoryList')->name('customer.get_category_list');
    Route::post('/get-product-model', 'CategoryController@getProductModel')->name('customer.get_product_model');

    // About us
    Route::get('/about-us', 'AboutController@index')->name('customer.aboutus');

    
});
