<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Hash;

class RandomHelper extends Controller
{
	public static function randomOTP($length = 6) 
	{
	    $pool = array_merge(range(0,9));
	    $key = "";
	    for($i=0; $i < $length; $i++) {
	        $key .= $pool[mt_rand(0, count($pool) - 1)];
	    }
	    return $key;
	}
	public static function randomKey($length = 8) 
	{
	    $pool = array_merge(range(0,9));
	    $key = "";
	    for($i=0; $i < $length; $i++) {
	        $key .= $pool[mt_rand(0, count($pool) - 1)];
	    }
	    return $key;
	}
	public static function randomID($length = 4) 
	{
	    $pool = array_merge(range(0,9));
	    $key = "";
	    for($i=0; $i < $length; $i++) {
	        $key .= $pool[mt_rand(0, count($pool) - 1)];
	    }
	    return $key;
	}
	public static function randomPassword($length = 8) 
	{
		// $special = '~!@#$%^&*(){}[],./?';
	    $pool = array_merge(range(0,9), range('a', 'z'));
	    $key = "";
	    for($i=0; $i < $length; $i++) {
	        $key .= $pool[mt_rand(0, count($pool) - 1)];
	    }
	    return $key;
	}
	public static function lat_lot($latitude, $longitude){
        $url='https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=true&key=AIzaSyBNu0dKE8etZBMEqkOpZsERwa0fqmgtjBQ';
        $json = @file_get_contents($url);
        $data=json_decode($json);
        $status = $data->status;
        if($status=="OK")
        {
            return $data->results[0]->formatted_address;
            // return $data->results[0]->address_components[2]->long_name;
        }
        else
        {
             return false;
        }
    }
}