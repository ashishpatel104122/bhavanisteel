<?php 
namespace App\Helpers;

use Illuminate\Database\Eloquent\Helper;
use AWS;
use App\Helpers\RandomHelper;
use App\Helpers\Exceptions;
use Illuminate\Support\Facades\Cache;

class SMSHelper
{
	public static function sendOTP($country_code="",$phone="")
	{
		try {
			$exp_time = 5;
			if(Cache::has($country_code))
			{
				$temp = Cache::get($country_code);
				$otp = $temp;
			} else{
				$otp = RandomHelper::randomOTP();
				Cache::put($country_code,$otp,$exp_time);
			}
			if(Cache::has($country_code))
			{
				$temp = Cache::get($country_code);
				$otp = $temp;
			} else{
				$otp = RandomHelper::randomOTP();
				Cache::put($country_code,$otp,$exp_time);
			}	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$message = " is the one time password (OTP) for Hawk Granito. This is usable once time and PLEASE DO NOT SHARE WITH ANYONE.";
		        $sendSMS = $sms->publish([
		            'Message' => $otp.$message,
		            'PhoneNumber' => $country_code.$phone,    
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        if($sendSMS){
			        $data['otp'] = $otp;
			        $data['phone'] = $country_code.$phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	public static function vcapOTP($country_code="",$phone="")
	{
		try {
			$exp_time = 5;
			if(Cache::has($country_code))
			{
				$temp = Cache::get($country_code);
				$otp = $temp;
			} else{
				$otp = RandomHelper::randomOTP();
				Cache::put($country_code,$otp);
			}
			if(Cache::has($country_code))
			{
				$temp = Cache::get($country_code);
				$otp = $temp;
			} else{
				$otp = RandomHelper::randomOTP();
				Cache::put($country_code,$otp);
			}	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$message = " is the one time password (OTP) for Cyber Siara. This is usable once time and PLEASE DO NOT SHARE WITH ANYONE.";
		        $sendSMS = $sms->publish([
		            'Message' => $otp.$message,
		            'PhoneNumber' => $country_code.$phone,    
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        if($sendSMS){
			        $data['otp'] = $otp;
			        $data['phone'] = $country_code.$phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	public static function sendUserIDPassword($country_code="",$phone="",$userid, $password)
	{
		try {	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$message = "Hawk Granito, Your user id ".$userid." and your password is ".$password;
		        $sendSMS = $sms->publish([
		            'Message' => $message,
		            'PhoneNumber' => $country_code.$phone,    
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        if($sendSMS){
			        $data['phone'] = $country_code.$phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	public static function check_in_out($country_code="",$phone="",$address="",$status="",$name="")
	{
		try {	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$message = "Hawk Granito, ".$name." status is ".$status." and location ".$address;
				$sendSMS = $sms->publish([
		            'Message' => $message,
		            'PhoneNumber' => $country_code.$phone,    
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        if($sendSMS){
			        $data['phone'] = $country_code.$phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	public static function PasswordRequest($country_code="",$phone="",$name="",$userid="")
	{
		try {	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$message = "Hawk Granito, ".$name." ( ".$userid." ) request for password change ";
				$sendSMS = $sms->publish([
		            'Message' => $message,
		            'PhoneNumber' => $country_code.$phone,    
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        if($sendSMS){
			        $data['phone'] = $country_code.$phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	public static function sendPO($country_code="",$phone="",$data="")
	{
		try {	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$link = $data['po_link'];
				$message = 'Hawk Granito, '.$data['name']." po is ".$link." is here.";
				if($data['gst_link']){
					$gst_link = $data['gst_link'];
					$message = 'Hawkio, '.$data['name']." po is ".$link." and gst image is ".$gst_link." is here.";
				}
		        $sendSMS = $sms->publish([
		            'Message' => $message,
		            'PhoneNumber' => $country_code.$phone,    
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        if($sendSMS){
			        $data['phone'] = $country_code.$phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	public static function register($customer)
	{
		try {	
			if($customer=="" || $customer->phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$message = " You are register successfully ";
				if(isset($customer->gst_image)){
					$link = $customer->link;
					$message = $message.$link;
				}
				// dd($message);
		        $sendSMS = $sms->publish([
		            'Message' => $message,
		            'PhoneNumber' => '91'.$customer->phone,    
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        if($sendSMS){
			        $data['phone'] = '91'.$customer->phone;
			        $data['message'] = $message;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	public static function RequestPo($country_code="",$phone="",$name="",$userid="")
	{
		try {	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$message = "Hawk Granito, ".$name." ( ".$userid." ) request for restriction remove";
				$sendSMS = $sms->publish([
		            'Message' => $message,
		            'PhoneNumber' => $country_code.$phone,    
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        if($sendSMS){
			        $data['phone'] = $country_code.$phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
}