<?php 
namespace App\Helpers;

use Illuminate\Database\Eloquent\Helper;

class ResponseMessage
{
	public static function success($msg,$data = array())
	{
		if(count(array($data))){
			echo json_encode(['status' => 1, 'message' => $msg, 'data' => $data ]);
		} else{
			echo json_encode(['status' => 1, 'message' => $msg, 'data' => null]);
		}
		exit;
	}

	public static function customeResponse($status,$msg,$data = array())
	{
		echo json_encode(['status' => $status, 'message' => $msg, 'data' => $data]);
		exit;
	}

	public static function error($msg,$data = array(),$extra=array())
	{
		if(count($extra)!=0){
			echo json_encode(['status' => 0, 'message' => $msg , 'data' => $data , array_keys($extra)[0]=>array_values($extra)[0]]) ; 	
		} else{
			if(count($data)!=0){
				echo json_encode(['status' => 0, 'message' => $msg , 'data' => $data]);
			} else {
				echo json_encode(['status' => 0, 'message' => $msg , 'data' => null]);
			}
		}
		exit;
	}

	public static function unauthorize($msg)
	{
		echo json_encode(['status' => 401, 'message' => $msg , 'data' => array()]);
		exit;
	}
}
?>