<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Auth;
use App\Shop\Modules\Module;
use App\Shop\Module\ModulePermission;
use App\Shop\Roles\Role;
use App\Shop\Roles\RoleUser;
use Log;

class Permission 
{
	public static function permission($module="") 
	{
	    try {
			$admin = Auth::guard('employee')->user();
			// dd($admin);
        	$permission = RoleUser::join('module_permission', 'module_permission.role_id', 'role_user.role_id')
                                ->join('module', 'module.id', 'module_permission.module_id')
                                ->select('module_permission.view','module_permission.add','module_permission.edit','module_permission.delete')
                                ->where('module.name', $module)  
                                // ->where('module_permission.role_id', $admin->role)
                                // ->where('module_permission.role_id', $admin->role)
                                ->where('role_user.role_id', $admin->role)
                                ->first();
			return $permission;
		} catch (Exception $e) {
			Log::error($e);	
		}
	}

}