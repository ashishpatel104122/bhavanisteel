<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\Schema;
use App\Http\Controllers\Customer\BasketController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Cashier::useCurrency(config('cart.currency'), config('cart.currency_symbol'));

        view()->composer('*', function ($view) {
          $basket = new BasketController();
          $data = $basket->getBasketDetails();

          $view->with('subTotal', $data->subTotal);
          $view->with('grandTotal', $data->grandTotal);
          $view->with('cart', $data->cart);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }
}
