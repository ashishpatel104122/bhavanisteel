<?php

namespace App\Http\Controllers\Auth;

use App\Shop\Customers\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Log;
use Redirect;
use Url;
use Hash;

class RegisterController extends Controller
{
    public function register(Request $request){
        try{
            $rules = [
                'name' => 'required|string|max:255',
                'organisation' =>'required|max:255',
                'address_line1' => 'required',
                'country' => 'required',
                'state'  => 'required|numeric',
                'city' => 'required|numeric',
                'post_code' =>'required|numeric|digits_between:6,6',
                'email' => 'required|string|email|max:255|unique:Customers,email',
                'pan_card' =>'required|max:50',
                'aadhar_card' =>'required|numeric|digits_between:12,12',
                'gst_no' =>'required|max:15|min:15',
                'cst_no' =>'required|max:11|min:11',
                'tin_no' =>'required|numeric|digits_between:9,12',
                'phone' => 'required|numeric|digits_between:10,10|unique:Customers,phone',
                'password' => 'required|min:6|max:50',
                'passwordCon' => 'required|min:6|max:50',
                'image' =>'required',
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
                'organisation.required' => 'Please enter organisation.',
                'address_line1.required' => 'Please enter address_line 1.',
                'address_line2.required' => 'Please enter address_line 2.',
                'country.required' => 'Please enter country.',
                'state.required' => 'Please enter state.',
                'city.required' => 'Please enter city.',
                'post_code.required' => 'Please enter post_code.',
                'email.required' => 'Please enter email address.',
                'email.email' => 'Invalid email address.',
                'email.unique' => 'Email is already exists.',
                'country_code.required' => 'Please enter country code.',
                'phone.required' => 'Please enter contact number',
                'phone.digits_between' => 'Please enter only digits and maximum 10 digits.',
                'phone.unique' => 'Contact number is already exists.',
                'post_code.digits_between' => 'Please enter only digits and maximum 6 digits.',
                'aadhar_card.digits_between' => 'Please enter only digits and maximum 12 digits.',
                'image.required' => 'Please select Image',
                'image.image' => 'Profile image type invalid.',
                'pan_card.required' => 'Please enter pan card number',
                'post_code.required' => 'Please enter post code',
                'aadhar_card.required' => 'Please enter aadhar card number.',
                'gst_no.required' => 'Please enter gst number.',
                'cst_no.required' => 'Please enter cst number.',
                'tin_no.required' => 'Please enter tin number.',
                'tin_no.digits_between' => 'Please enter only digits and maximum 12 digits.',
            ];
            $password = trim(strip_tags($request->password));
            $passwordCon = trim(strip_tags($request->passwordCon));
            if ($password != $passwordCon) {
                $rules['password'] = 'required|confirmed';
            }
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{
                $name = trim(strip_tags($request->name));
                $email = trim(strip_tags($request->email));
                $country_code = "91";
                $phone = trim(strip_tags($request->phone));
                $password = trim(strip_tags($request->password));
                $password_hash = Hash::make($password);
                $organisation = trim(strip_tags($request->organisation));
                $address_line1 = trim(strip_tags($request->address_line1));
                $address_line2 = trim(strip_tags($request->address_line2));
                $country = trim(strip_tags($request->country));
                $state = trim(strip_tags($request->state));
                $city = trim(strip_tags($request->city));
                $post_code = trim(strip_tags($request->post_code));
                $pan_card = trim(strip_tags($request->pan_card));
                $aadhar_card = trim(strip_tags($request->aadhar_card));
                $gst_no = trim(strip_tags($request->gst_no));
                $cst_no = trim(strip_tags($request->cst_no));
                $tin_no = trim(strip_tags($request->tin_no));
                $notes = trim(strip_tags($request->notes));

                if($request->id!=""){
                    $customer = Customer::find($request->id);
                } else{
                    $customer = New Customer;
                }
                $customer->name = $name;
                $customer->email = $email;
                $customer->password = $password_hash;
                $customer->country_code = $country_code;
                $customer->organisation = $organisation;
                $customer->address_line1 = $address_line1;
                $customer->country = $country;
                $customer->state = $state;
                $customer->city = $city;
                $customer->post_code = $post_code;
                $customer->pan_card = $pan_card;
                $customer->aadhar_card = $aadhar_card;
                $customer->gst_no = $gst_no;
                $customer->cst_no = $cst_no;
                $customer->tin_no = $tin_no;
                $customer->phone = $phone;
                $customer->notes =$notes;
                $customer->is_active = "1";
                if($customer->save()) {
                    if ($request->has('image')) {
                        $time      = md5(time());
                        $file      = $request->image;
                        $extension = $file->getClientOriginalExtension();
                        $profile   = $time . '.' . $extension;
                        $file->move(public_path('customers/profile/'), $profile);
                        $profile                 = 'customers/profile/'.$profile;
                        $profile_update          = Customer::find($customer->id);
                        $profile_update->profile = $profile;
                        $profile_update->save();
                    }
                    return redirect()->to('/login')->with('success','Please enter valid email or password.');
                } else{
                    ResponseMessage::error('Something was wrong please try again.');
                }
            }
        } catch (Exception $e) {
            log::debug($e);
        }   
    }
}
