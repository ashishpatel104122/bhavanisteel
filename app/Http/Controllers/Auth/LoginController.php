<?php

namespace App\Http\Controllers\Auth;

use App\Shop\Admins\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Shop\Countries\Country;
use App\Shop\States\State;
use App\Shop\Cities\City;
use Log;
use Auth;
use Session;

class LoginController extends Controller
{
    public function showLoginForm(Request $request)
    {
        try{
            $country = Country::select('id')->where('country','india')->first();
            $data['states'] =   State::where('country_id',$country->id)->get();
            return view('auth.login')->with($data);
        } catch (Exception $e) {
            log::debug($e);
        } 
    }

    public function login(Request $request)
    {
        try{
            $rules = [
                'email' => 'required|email|max:50',
                'password' =>'required|min:6|max:50',
            ];
            $customeMessage = [
                'email.required' => 'Please enter email.',
                'email.email' => 'Please enter valid email.',
                'password.required' => 'Please enter password.',
            ];
            if(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'is_active'=>'1'])){
                return redirect()->to('/');
            } elseif(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'is_active'=>0])){
                $request->session()->invalidate();
                return redirect('/login')->with('error','Your Account is not active');
            }
            return redirect()->back()->withinput($request->only('email','remember'))->with('error','Please enter valid email or password.');
        } catch (Exception $e) {
            log::debug($e);
        } 
    }
    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->to('/');
    }
    
}
