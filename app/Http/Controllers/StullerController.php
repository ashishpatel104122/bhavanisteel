<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop\Products\Product;

class StullerController extends Controller
{
    public function index(Request $request)
    {    	
    	dd(Product::get());
    	try {
    		if(isset($request->cid)){
	            set_time_limit(0);
	            $CategoryIds = $request->cid;
	            $curl = curl_init();
	            $header = array(
	                "authorization: Basic ZW5jb3JlZW50OkVuY29yZTUwMDUh",
	                "cache-control: no-cache",
	                "content-type: application/json",
	              );
	            if(isset($request->nextpage)!=""){
	                $postfield =  '{"Include":["All"],"Filter":[    "Orderable",      "OnPriceList",      "InStock"   ],"CategoryIds":['.$CategoryIds.'],"AdvancedProductFilters":[  {  "Type":"ProductType","Values":[ {  "DisplayValue":"Rings","Value":"Rings"}]}],"NextPage": "'.$request->nextpage.'" }';
	            } else{
	                $postfield =  '{"Include":["All"],"Filter":[        "Orderable",      "OnPriceList",      "InStock"   ],"CategoryIds":['.$CategoryIds.'],"AdvancedProductFilters":[  {  "Type":"ProductType","Values":[ {  "DisplayValue":"Rings","Value":"Rings"}]}]}';
	            }
	            
	            curl_setopt_array($curl, array(
	                    CURLOPT_URL => "https://api.stuller.com/v2/products",
	                    CURLOPT_RETURNTRANSFER => true,
	                    CURLOPT_ENCODING => "",
	                    CURLOPT_MAXREDIRS => 10,
	                    CURLOPT_TIMEOUT => 0,
	                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	                    CURLOPT_CUSTOMREQUEST => "POST",
	                    CURLOPT_POSTFIELDS => $postfield,
	                    CURLOPT_HTTPHEADER => $header,
	                )
	            );

	            $response = curl_exec($curl);
	            $err = curl_error($curl);

	            curl_close($curl);

	            if ($err) {
	                echo "cURL Error #:" . $err;
	            } else {
	                $data = json_decode($response);
	                $tempQueryValues = "";
	                $ProductsCount = count($data->Products);
	                
	                if(isset($data->NextPage)){
	                    $nextPageKey = $data->NextPage;
	                }
	                
	                for ($i=0; $i < $ProductsCount; $i++) { 
	                    $description = $data->Products[$i]->Description;
	                    $GroupDescription = $data->Products[$i]->GroupDescription;
	                    $cover = $data->Products[$i]->GroupImages[0]->ThumbnailUrl;
	                    $price = $data->Products[$i]->Price->Value;
	                    $weight = $data->Products[$i]->Weight;
	                    $mass_unit = $data->Products[$i]->WeightUnitOfMeasure;

	                    $pid = $data->Products[$i]->Id;
	                    $sku = $data->Products[$i]->SKU;
	                    $WebCategoriesCount = count($data->Products[$i]->WebCategories);
	                    $tempCategories = array();
	                    for ($j=0; $j < $WebCategoriesCount ; $j++) { 
	                        $tempArray = $data->Products[$i]->WebCategories[$j]->Id;
	                        array_push($tempCategories, $tempArray);
	                    }
	                    $json_encode = json_encode($data->Products[$i]);
	                    $tempCategories = json_encode($tempCategories);
	                    $date = date('Y-m-d H:i:s');



	                    // if(Stuller::where('sku',$sku)->exists()){
	                    //     $stullerId = Stuller::where('sku',$sku)->first();
	                    //     $stuller = Stuller::find($stullerId);
	                    // } else{
	                    $stuller = New Stuller;
	                    $stuller->id = $pid;
	                    $stuller->category_id = $CategoryIds;
	                    $stuller->sub_category_id = $tempCategories;
	                    $stuller->brand_id = null;
	                    $stuller->sku = $sku;
	                    $stuller->name = $description;
	                    $stuller->slug = ucwords(str_replace("-"," ",$description));
	                    $stuller->description = $GroupDescription;
	                    $stuller->cover = $cover;
	                    $stuller->quantity = null;
	                    $stuller->price = $price;
	                    $stuller->sale_price = $price;
	                    $stuller->status = 1;
	                    $stuller->is_stuller = "yes";
	                    $stuller->length = null;
	                    $stuller->width = null;
	                    $stuller->height = null;
	                    $stuller->distance_unit = null;
	                    $stuller->weight = $weight;
	                    $stuller->mass_unit = strtolower($mass_unit);
	                    $stuller->gender = null;
	                    $stuller->is_featured = 0;
	                    dd($stuller);


	                    $stuller->request = $postfield;
	                    $stuller->response = $json_encode;
	                    $stuller->created_at = $date;
	                    $stuller->pid = $pid;
	                    $stuller->sku = $sku;
	                    $stuller->webcategories = $tempCategories;
	                    $stuller->categoryids = $CategoryIds;

	                    dd($stuller);
	                    $stuller->save();



	                    // if($tempQueryValues==""){
	                    //     $tempQueryValues .= "('$postfield','$json_encode','$date','$pid','$sku','$tempCategories','$CategoryIds')";
	                    // } else{
	                    //     $tempQueryValues .= ",('$postfield','$json_encode','$date','$pid','$sku','$tempCategories','$CategoryIds')";
	                    // }

	                    // echo($pid."<br>");

	                    // $sql = "INSERT INTO product(request,response,created_at,pid,sku,webcategories,categoryids) VALUES ('$postfield','$json_encode','$date','$pid','$sku','$tempCategories','$CategoryIds')";
	                    // if ($con->query($sql) === TRUE) {
	                    //  echo($pid."<br>");
	                    //  // echo("end=".date('H:m:i'));
	                    // } else{
	                    //  echo("NOT ".$pid."<br>");
	                    // }
	                    // sleep(10);
	                }

	                if(isset($nextPageKey) !=""){
	                    return redirect()->route('stuller.api',['cid'=>$CategoryIds,'nextpage'=>$nextPageKey]);
	                } else{
	                    return "success";
	                }
	            }
            } else{
            	return "please send cid";
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
