<?php

namespace App\Http\Controllers\Admin\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Shop\News\News;
use Validator;
use DB;
use Auth;
use Image;
use App\Helpers\Permission;


class NewsController extends Controller
{
    public function index(Request $request){
        try{
            $permission = Permission::permission('news');
            if($permission->view==1 || $permission->add==1){
                $page   = trim($request->page);
                $data['news'] = DB::table('news')
                        ->select('news.*','categories.name as category_name')
                        ->leftjoin('categories','categories.id','=','news.category_id')
                        ->OrderBy('news.id','desc')
                        ->get();
                return view('admin.news.list')->with($data);
            } else{
                return view('layouts.errors.403');    
            }
        }
        catch(Exception $e){
                $Exceptions = New Exceptions;
                $Exceptions->sendException($e);  
        }
    }
    public function create(){
        try{
            $permission = Permission::permission('news');
            if($permission->add==1){
                $data['categories'] = Category::select('id','name')->get();
                    return view('admin.news.create')->with($data);
            } else{
                return view('layouts.errors.403');
            }
        }
        catch(Exception $e){
                $Exceptions = New Exceptions;
                $Exceptions->sendException($e);  
            }
    }
    public function store(Request $request){
        try{
            $rules =[
                'category_id' => 'required',
                'name' => 'required|string|max:255',
                'size' => 'required|max:50',
            ];
            $customeMessage = [
                'category_id.required' => 'Please select category.',
                'name.required' => 'Please enter name.',
                'size.required' => 'Please enter size.',
                'background_image.required' => 'Please select background image.',
                'background_image.image' => 'Please select valid image',
            ];
            if($request->id==""){
                $rules['background_image'] ='required|image';
            }
            $validator = Validator::make($request->all(),$rules,$customeMessage);
                if( $validator->fails() ){
                    return back()->withInput()->withErrors($validator->errors());
                } else{  
                    $category_id = trim(strip_tags($request->category_id));
                    $name = trim(strip_tags($request->name));
                    $size = trim(strip_tags($request->size));
                }
                if($request->id!=""){
                    $news=News::find($request->id);
                }else{
                    $news=New News;
                }
                $news->category_id = $category_id;
                $news->name = $name;
                $news->size = $size;
                if($news->save()){
                    if ($request->has('background_image')) {
                        $time      = md5(time());
                        $file      = $request->background_image;
                        $image     = $request->file('background_image');
                        $extension = $file->getClientOriginalExtension();
                        $background_image   = $time . '.' . $file->getClientOriginalExtension();
                        $destinationPath = public_path('image/news/');
                        $destinationPathCompress = public_path('image/news/480X480/');
                         // 480X480 image save
                        $resize = Image::make($image->getRealPath());
                        $resize->resize(480, 480, function ($constraint) {
                        $constraint->aspectRatio();
                        })->save($destinationPathCompress.'/'.$background_image);

                        $file->move($destinationPath, $background_image);
                        $background_image                 = 'image/news/' . $background_image;
                        $background_image_update          = News::find($news->id);
                        $background_image_update->background_image = $background_image;
                        $background_image_update->save();
                    }
                }
                if($request->id!=""){
                    return redirect()->route('admin.news.list')->with('message', 'News updated successfully.');
                }
                else{
                    return redirect()->route('admin.news.list')->with('message', 'News create successfully.');
                }
        }
        catch(Exception $e){
                $Exceptions = New Exceptions;
                $Exceptions->sendException($e);  
            }
    }
    public function destroy($id)
    {
        try{
            $permission = Permission::permission('news');
            if($permission->delete==1){
                $news_delete = News::where('id',$id);
                $news_delete->delete();
                return redirect()->route('admin.news.list')->with('message', 'News delete successfully.');     
            } else{
                return view('layouts.errors.403');
            }
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function edit($id)
    {
        try{
            $permission = Permission::permission('news');
            if($permission->edit==1){
                if(News::where('id',$id)->exists()){  
                    $data['categories'] = Category::select('id','name')->get();
                    $data['news'] = News::where('id',$id)->first();
                    return view('admin.news.create')->with($data);    
                }
            } else{
                return view('layouts.errors.403');
            }
        }
        catch(Exception $e){
            $Exceptions = New Exceptions;
            $Exceptions->sendException($e);  
        }
    } 
    public function status($id)
    {
        try{
            $user = Auth::guard('employee')->user();
            if ($user->hasPermission('manage-news')) {
                if (News::where('id',$id)->exists()) {
                    $News = News::where('id',$id)->first();
                    if ($News->status==1) {
                        $News->status = 0;
                    } elseif($News->status==0){
                        $News->status = 1;
                    }
                    $News->save();
                    return redirect()->route('admin.news.list')->with('message', 'News status updated successfully');
                }
            } else{
                return view('layouts.errors.403');
            }    
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
}
