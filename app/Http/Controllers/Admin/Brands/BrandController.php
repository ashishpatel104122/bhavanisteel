<?php

namespace App\Http\Controllers\Admin\Brands;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Shop\Brands\Brand;
use Validator;
use Auth;
use Log;
use App\Helpers\Permission;

class BrandController extends Controller
{
    public function index(Request $request)
    {
        try{
            $permission = Permission::permission('brands');
            if($permission->view==1 || $permission->add==1){
                $page   = trim($request->page);
                $data['brands'] = Brand::OrderBy('id','desc')->get();
                return view('admin.brands.list')->with($data);
            } else{
                return view('layouts.errors.403');
            }
        }
        catch (Exception $e) {
            log::debug($e);
        }
    }
    public function create()
    {
        try {
            $permission = Permission::permission('brands');
            if($permission->add==1){
                return view('admin.brands.create');
            }else{
                return view('layouts.errors.403');
            }
        } catch (Exception $e) {
            Log::errors($e);
        }
        
    }
    public function store(Request $request)
    {
        try {
            $rules =[
                'name' => 'required|string|max:255',
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{  
                $name = trim(strip_tags($request->name));
                $status    = $request->status;
            }
            if($request->id!=""){
                $brands=brand::find($request->id);
            }else{
                $brands=New brand();
            }
            $brands->name = $name;
            $brands->status = $status;
            $brands->save();
            return redirect()->route('admin.brands.index')->with('message', 'Brand create successfully.');
        } catch (Exception $e) {
            Log::errors($e);
        }
    }
    public function edit($id){
        try{
            $permission = Permission::permission('brands');
            if($permission->edit==1){
                if(brand::where('id',$id)->exists()){  
                    $data['brand'] = brand::where('id',$id)->first();
                    return view('admin.brands.create')->with($data);    
                }
            } else{
                return view('layouts.errors.403');
            }
        }
        catch(\Exception $e){
            Log::error($e);         
        }
    }
    public function destroy($id)
    {
        try {
            $permission = Permission::permission('brands');
            if($permission->delete==1){
                $brand_delete = brand::where('id',$id);
                $brand_delete->delete();
                return redirect()->route('admin.brands.index')->with('message', 'Brand delete successfully.');
            } else{
                return view('layouts.errors.403');
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
