<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Shop\Categories\Category;
use Validator;
use Auth;
use Log;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        try{
            $data['categories'] = Category::select('categories.*')->orderBy('id','desc')->get();
            return view('admin.categories.list')->with($data);
        }
        catch (Exception $e) {
            log::debug($e);
        }
    }
    public function create()
    {
        try{
            $data['parent_categorys'] = Category::where('parent_id',0)->get();
            return view('admin.categories.create')->with($data);
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function store(Request $request)
    {
        try{
            $rules =[
                'name' => 'required|string|max:255',
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{  
                $name = trim(strip_tags($request->name));
                $parent_id = trim(strip_tags($request->parent_id));
            }
            if($request->id!=""){
                $category=Category::find($request->id);
            }else{
                $category=New Category();
            }
            $category->name = $name;
            $category->parent_id = $parent_id;
            $category->save();
            if($request->id!=""){    
                return redirect()->route('admin.categories.index')->with('message', 'Category Update Succesfully.');
            }
            else{
                return redirect()->route('admin.categories.index')->with('message', 'Category Create Succesfully.');
            }
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function edit($id)
    {
        try{
            if(Category::where('id',$id)->exists()){  
                $data['parent_categorys'] = Category::where('parent_id',0)->where('id','!=',$id)->get();
                $data['categories'] = Category::where('id',$id)->first();
                return view('admin.categories.create')->with($data);    
            }
        }
        catch(\Exception $e){
            $Exceptions = New Exceptions;
            $Exceptions->sendException($e);  
        }
    }
    public function destroy(int $id)
    {
        try{
            $cat_delete = Category::where('id',$id);
            $cat_delete->delete();
            return redirect()->route('admin.categories.index')->with('message', 'Category delete successfully.');
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
}

    
