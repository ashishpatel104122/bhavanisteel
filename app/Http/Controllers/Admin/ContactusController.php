<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Cms\ContactUs;

class ContactusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['contactus'] = ContactUs::where('cms_id',0)->get();
            return view('admin.contactus.list')->with($data);
        } catch (Exception $e) {
            
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contactus_delete = ContactUs::where('id',$id);
        $contactus_delete->delete();
        return redirect()->route('admin.contactus.list')->with('message', 'Enquiry delete successfully.');
    }
}
