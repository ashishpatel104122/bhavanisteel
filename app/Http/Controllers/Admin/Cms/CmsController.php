<?php

namespace App\Http\Controllers\Admin\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;
use DB;
use Validator;
use App\Shop\Cms\Cms;
use App\Shop\Cms\ContactUs;
use Exception;


class CmsController extends Controller
{
    public function index()
    {
    	try {
	    	$data['cms'] = DB::table('cms')->get();
	    	return view('admin.cms.list')->with($data);
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
    public function create()
    {
    	try {
    		return view('admin.cms.create');
    	} catch (Exception $e) {
    		Log::error($e);	
    	}
    }
    public function store(Request $request)
    {
        try {
            $rules =[
                'title' => 'required',
                'type' => 'required',
            ];
            $customeMessage = [
                'title.required' => 'Please select category.',
                'type.required' => 'Please enter type.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);   
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{  
                $title = trim(strip_tags($request->title));
                $type = trim(strip_tags($request->type));
                $description = trim(strip_tags($request->description));
                $email = trim(strip_tags($request->email));
                $address = trim(strip_tags($request->address));
                $phone =  $request->phone_no;    

                if($request->id!=""){
                    $cms=Cms::find($request->id);
                }else{
                    $cms= New Cms();
                }
                $cms->title = $title;
                $cms->type = $type;
                $cms->description = $description;                 
                if($cms->save()){
                    if ($request->type=="contact_us") {
                        if($request->id!=""){
                            $contact_us=ContactUs::where('cms_id',$request->id)->first();
                        }else{
                            $contact_us= New ContactUs();
                        }
                        $contact_us->cms_id = $cms->id;
                        $contact_us->email = $email;
                        $contact_us->address = $address;
                        $contact_us->phone_no =  implode(',', $phone);
                        $contact_us->save();
                    }
                }
                if($request->id!=""){
                    return redirect()->route('admin.cms.list')->with('message', 'Cms updated successfully.');
                }else{
                    return redirect()->route('admin.cms.list')->with('message', 'Cms create successfully.');
                }
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
    public function edit($id)
    {
        try{
            $data['cms'] = Cms::where('id',$id)->first();
            $data['contact_us'] = ContactUs::where('cms_id',$id)->first();
            return view('admin.cms.create')->with($data);        
        }
        catch(Exception $e){
            Log::error($e);
        }
    }
    public function destroy($id)
    {
        try{
            $cms_delete = Cms::where('id',$id);
            $cms_delete->delete();
            return redirect()->route('admin.cms.list')->with('message', 'Cms delete successfully.');
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
}
