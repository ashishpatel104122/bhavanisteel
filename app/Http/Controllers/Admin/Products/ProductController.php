<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Products\ProductImage;
use Illuminate\Http\Request;
use Image;
use Validator;
use DB;
use Session;
use Auth;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        try{
            $page   = trim($request->page);
            $data['products'] = DB::table('products')
                ->select('products.*','categories.name as  categories_name')
                ->leftjoin('categories','categories.id','=','products.category_id')
                ->orderBy('products.id','desc')
                ->get();
            return view('admin.products.list')->with($data);
        }
        catch (Exception $e) {
            Log::debug($e);
        }
    }
    public function create(Request $request)
    {
        try{    
            $data['categories'] = Category::select('id','name')->where('parent_id',0)->get();
            return view('admin.products.create')->with($data);
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function store(Request $request)
    {
        try{
            $rules =[
                'category_id' => 'required',
                'name' => 'required|string|max:255',
            ];
            $customeMessage = [
                'category_id.required' => 'Please select category.',
                'name.required' => 'Please enter name.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);   
                if( $validator->fails() ){
                    
                    return back()->withInput()->withErrors($validator->errors());
                } else{  
                    $category_id = trim(strip_tags($request->category_id));
                    $sub_category_id = trim(strip_tags($request->sub_category_id));
                    $name = trim(strip_tags($request->name));
                    $description = $request->description;
                    $status = $request->status;    
                    $price = $request->price; 
                    if ($request->key!="") {
                        $key = implode(',', $request->key); 
                    }
                    if ($request->value!="") {
                        $value = implode(',', $request->value); 
                    }
                    if($request->id!=""){
                        $product=Product::find($request->id);
                    }else{
                        $product= New Product();
                    }
                    $product->category_id = $category_id;
                    $product->sub_category_id = $sub_category_id;
                    $product->name = $name;
                    $product->description = $description;
                    $product->status = $status;
                    $product->price = $price;
                    $product->key = $key;
                    $product->value = $value;
                    if($product->save()){
                        if ($request->background_image) {
                            foreach ($request->background_image as $key => $background_image) {
                                $time      = md5(round(microtime(true) * 1000));
                                $file      = $background_image;
                                $extension = $file->getClientOriginalExtension();
                                $background_image   = $time . '.' . $file->getClientOriginalExtension();
                                $file->move(public_path('background_image/'), $background_image);
                                $background_image        = 'background_image/'. $background_image;
                                $product_update          = new ProductImage();
                                $product_update->product_id = $product->id;
                                $product_update->image = $background_image;
                                $product_update->save();

                                
                                $product_update          = Product::find($product->id);
                                $product_update->background_image = $background_image;
                                $product_update->save();
                                
                            }
                        }
                    }
                    if($request->id!=""){
                        return redirect()->route('admin.products.index')->with('message', 'Product updated successfully.');
                    }else{
                        return redirect()->route('admin.products.index')->with('message', 'Product create successfully.');
                    }
                }
            }
        catch (Exception $e) {
            Log::error($e);
        }
    }
    public function edit($id)
    {
        try{
            $data['categories'] = Category::select('id','name')->where('parent_id',0)->get();
            $data['products'] = Product::where('id',$id)->first();
            $data['products_images'] = ProductImage::where('product_id',$id)->get();
            return view('admin.products.create')->with($data);        
        }
        catch(Exception $e){
            $Exceptions = New Exceptions;
            $Exceptions->sendException($e);  
        }
    }
    public function destroy($id)
    {
        try{
            $product_delete = Product::where('id',$id);
            $product_delete->delete();
            return redirect()->route('admin.products.index')->with('message', 'Product delete successfully.');
        }
        catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function removeThumbnail($id)
    {
        try{
            if(ProductImage::where('id',$id)->exists()){
                $image_delete = ProductImage::where('id',$id)->delete();
                return redirect()->back()->with('message', 'Image remove successfully.');
            }
            else{
                return back();
            }
        }
        catch(Exception $e){
            $Exceptions = New Exceptions;
            $Exceptions->sendException($e);  
        }
    }
    public function getCategory(Request $request)
    {
        try {
            if (Category::where('parent_id',$request->category_id)->exists()) {
                $categories = Category::select('id','name')->where('parent_id',$request->category_id)->get();
                $data = [];
                foreach( $categories as $category )
                {
                    $data[$category->id] = $category->name;
                }
                return $data;
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
