<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\User;

class UserController extends Controller
{
    public function index()
    {
    	$data['users'] =  User::get();
    	$data['test'] ="test";
    	return view('admin.users.index')->with($data);
    }
}
