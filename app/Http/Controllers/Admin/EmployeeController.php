<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Shop\Employees\Employee;
use Illuminate\Http\Request;
use Log;
use Validator;
use Carbon\Carbon;
use Auth;

class EmployeeController extends Controller
{
    public $rules = [
        'first_name'    => 'required',
        'last_name' => 'required',
        'role' => 'required',
    ];

    public $customeMessage = [
        'first_name.required'    => 'Please enter First name.',
        'last_name.required' => 'Please enter Last name.',
        'email.required'   => 'Please enter Email.',
        'email.email' => 'Please enter valid Email address',
        'email.unique' =>'Email address already in use',
        'password.required'   => 'Please enter Password.',
        'password.min'   => 'Please enter minimum 6 character or number.',
        'role.required'   => 'Please select Role.',
    ];
    
    public function edit($id)
    {
        try{
            $permission = Permission::permission('roles');
            if($permission->edit==1){
                $data['roles'] = Role::where('deleted_at',null)->get();
                $data['employees'] = Employee::where('deleted_at',null)->where('id',$id)->first();
                return view('admin.employees.create')->with($data);
            }else{
                return view('layouts.errors.403');    
            }
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
    
    public function getProfile($id)
    {
        try{
            $data['employee'] = Employee::where('id',$id)->first();
            return view('admin.employees.profile')->with($data);
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
    public function updateProfile(Request $request, $id)
    {
        try{
            $first_name = $request->first_name;
            $last_name = $request->last_name;
            $email = $request->email;
            $username = $request->username;
            $old_password = $request->old_password;
            $password = $request->password;
            $password_confirmation = $request->password_confirmation;
            $employee = Employee::where('id',$id)->first();
            if ($request->has('password') && $request->input('password') != '') {
                if(!Hash::check($old_password, $employee->password)){
                    return back()->with('error','The specified password does not match the database password');
                } else{
                    $password_hash = Hash::make($password);
                    $employee->password = $password_hash;        
                }
            }
            $employee->first_name = $first_name;
            $employee->last_name = $last_name;
            $employee->email = $email;
            $employee->username = $username;
            if($employee->save()){
                if ($request->has('profile_image')) {
                    $time      = md5(time());
                    $file      = $request->profile_image;
                    $extension = $file->getClientOriginalExtension();
                    $profile   = $time . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('employee/profile/'), $profile);
                    $profile                 = 'employee/profile/' . $profile;
                    $profile_update          = Employee::find($employee->id);
                    $profile_update->profile_image = $profile;
                    $profile_update->save();
                }
            }
            return redirect()->route('admin.employee.profile', $id)
                ->with('message', 'Profile update  successfully.');
        } catch ( Exception $e) {
            Log::debug($e);
        }
    }
}
