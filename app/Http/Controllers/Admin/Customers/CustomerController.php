<?php

namespace App\Http\Controllers\Admin\Customers;

use App\Shop\Customers\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Helpers\RandomHelper;
use Auth;
use Log;
use Validator;
use App\Helpers\ResponseMessage;
use App\Shop\Dealers\Dealer;
use App\Shop\Customers\ForgetPassword;
use App\Shop\Cities\City;
use App\Shop\States\State;
use App\Helpers\MailHelper;
use App\Helpers\SMSHelper;
use App\Helpers\Permission;
use App\Shop\Customers\RequestPo;
use Carbon\Carbon;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        try{
            $permission = Permission::permission('users');
            if($permission->view==1 || $permission->add==1){
                $page=trim($request->page);
                $data['customers'] = DB::table('customers')
                        ->select('customers.*', 'states.name as state_name', 'cities.name as city_name', 'country.country as country_name')
                        ->leftjoin('country','country.id','customers.country')
                        ->leftjoin('states','states.id','customers.state')
                        ->leftjoin('cities','cities.id','customers.city')
                        ->OrderBy('customers.created_at','desc')
                        ->where('customers.deleted_at','=',null)
                        ->get();
                return view('admin.customers.list')->with($data);
                
            }else{
                return view('layouts.errors.403');
            }
        }
        catch(Exception $e){
                $Exceptions = New Exceptions;
                $Exceptions->sendException($e);  
        }
    }
    public function create()
    {
        try{
            $permission = Permission::permission('users');
            if($permission->add==1){
                $pass = RandomHelper::randomPassword();
                $rand = RandomHelper::randomID();
                $name = 'Hawk@'.$rand;
                return view('admin.customers.create',[
                    'password' => $pass,
                    'name' => $name
                ]);
            } else{
                return view('layouts.errors.403');
            }    
        }
        catch(Exception $e){
                $Exceptions = New Exceptions;
                $Exceptions->sendException($e);  
        }
    }
    public function edit($id) {
        try {
            $permission = Permission::permission('users');
            if($permission->edit==1){
                $customer = Customer::find($id);
                $data['customer'] = $customer;
                if($customer->city){
                    $data['cities'] = City::where('state_id', $customer->state)->get();
                }
                $data['states'] = State::get();
                return view('admin.customers.edit',$data);
            } else{
                return view('layouts.errors.403');
            }  
        } catch (Exception $e) {
            Log::errors($e);
        }
    }
    public function store(Request $request){
        try {
            $rules = [
                'user_id' => 'required|unique:customers',
                'phone' => 'required|numeric|unique:customers',
                'email' => 'required|unique:customers',
            ];
            $customeMessage = [
                'user_id.required' => 'Please enter user id.',
                'user_id.unique' => 'User id is already exists.',
                'phone.required' => 'Please enter user id.',
                'phone.unique' => 'Contact number is already exists.',
                'phone.numeric' => 'Please enter number',
                'email.required' => 'Please enter user id.',
                'email.unique' => 'Email is already exists.',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                return back()->withInput()->withErrors($validator->errors());
            } else {

                $customer = new Customer();
                $customer->user_id = $request->user_id;
                $customer->phone = $request->phone;
                $customer->password = $request->password;
                $customer->email = $request->email;
                $customer->is_active = $request->status;
                $customer->role = $request->role;
                $customer->organisation = "HAWK GRANITO PVT. LTD.";
                $customer->is_restricted = $request->is_restricted;
                $customer->status = 0;
                if($customer->save()){
                    if ($request->has('images')) {
                            $time      = md5(time());
                            $file      = $request->images;
                            $extension = $file->getClientOriginalExtension();
                            $image   = $time . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path('customers/profile/'), $image);
                            $image                 = 'customers/profile/' . $image;
                            $image_update          = Customer::find($customer->id);
                            $image_update->profile = $image;
                            $image_update->save();
                    }       
                    $result = MailHelper::userIDMail($customer);
                    $sms = SMSHelper::sendUserIDPassword('91',$customer->phone,$customer->user_id,$customer->password );
                    return redirect()->route('admin.customers.index')->with('message', 'Customer add successfully.');
                }
            }
        } catch (Exception $e) {
            $Exceptions = New Exceptions;
            $Exceptions->sendException($e);
        }
    }
    public function update(Request $request, $id){
        try {
            $rules = [];
            $customeMessage = [
                'phone.required' => 'Please enter user id.',
                'phone.unique' => 'Contact number is already exists.',
                'phone.numeric' => 'Please enter number',
                'email.required' => 'Please enter user id.',
                'email.unique' => 'Email is already exists.',
            ];
            $customer_id = trim(strip_tags($id));
            if(Customer::where('id',$customer_id)->exists()){
                $customer = Customer::where('id',$customer_id)->get()->first();
                if($customer->phone!=trim(strip_tags($request->phone))){
                    $rules['phone'] = 'required|numeric|unique:customers';
                }
                if($customer->email!=trim(strip_tags($request->email))){
                    $rules['email'] = 'required|unique:customers';
                }
            }
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                return back()->withInput()->withErrors($validator->errors());
            } else {
                // dd($request->all());
                if(Customer::find($id)->exists()) {
                    $customer = Customer::find($id);
                    $customer->phone = $request->phone;
                    $customer->role = $request->role;
                    if($request->password){
                        if($customer->password != $request->password){
                            $result = MailHelper::userIDMail($customer);
                            $sms = SMSHelper::sendUserIDPassword('91',$customer->phone,$customer->user_id,$customer->password );
                            ForgetPassword::where('userid',$customer->user_id)->delete();
                        }
                    }
                    $customer->email = $request->email;
                    $customer->is_active = $request->is_active;

                    $customer->name = $request->name;
                    $customer->country_code = 91;
                    $customer->organisation = $request->organisation;
                    $customer->address_line1 = $request->address_line1;
                    $customer->address_line2 = $request->address_line2;
                    $customer->country = $request->country;
                    $customer->state = $request->state;
                    $customer->city = $request->city;
                    $customer->post_code = $request->zip;
                    $customer->aadhar_card = $request->aadhar_card;
                    if (isset($request->is_restricted)) {
                        $customer->is_restricted = 0; 
                        $customer->restricted_time = null;
                    } else{
                        $date = date_default_timezone_set('Asia/Kolkata');
                        $customer->is_restricted = "1";
                        $customer->restricted_time = date('Y-m-d H:i:s');
                        if (RequestPo::where('customer_id',$customer->id)->exists()) {
                            RequestPo::where('customer_id',$customer->id)->delete();
                        }
                    }
                    if($request->gst_no){
                        $customer->gst_no = $request->gst_no;
                    } else {
                        $customer->gst_no = null;
                    }
                    $customer->status = $request->status;
                    $customer->notes =$request->notes;
                    if($customer->save()){
                        if ($request->has('background_image')) {
                            $time      = md5(time());
                            $file      = $request->background_image;
                            $extension = $file->getClientOriginalExtension();
                            $background_image   = $time . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path('customers/gst/'), $background_image);
                            $background_image                 = 'customers/gst/' . $background_image;
                            $background_image_update          = Customer::find($customer->id);
                            $background_image_update->gst_image = $background_image;
                            $background_image_update->save();
                        }
                        if ($request->has('images')) {
                            $time      = md5(time());
                            $file      = $request->images;
                            $extension = $file->getClientOriginalExtension();
                            $image   = $time . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path('customers/profile/'), $image);
                            $image                 = 'customers/profile/' . $image;
                            $image_update          = Customer::find($customer->id);
                            $image_update->profile = $image;
                            $image_update->save();
                        } 
                        return redirect()->route('admin.customers.index')->with('message', 'Customer edit successfully.');
                    }
                }
            }
        } catch (Exception $e) {
            $Exceptions = New Exceptions;
            $Exceptions->sendException($e);
        }
    }
    public function destroy($id)
    {
        try{
            $permission = Permission::permission('users');
            if($permission->delete==1){
                if (Customer::where('id',$id)->exists()) {
                    Customer::where('id',$id)->delete();
                }
                return redirect()->route('admin.customers.index')->with('message', 'Customer delete successfully.');
            } else{
                return view('layouts.errors.403');
            }  
        }
        catch(Exception $e){
                $Exceptions = New Exceptions;
                $Exceptions->sendException($e);  
        }
    }
    public function check_email(Request $request) {
        $email = Input::post('email');
        $id = Input::post('id');
        if ($email != '') {
            if (!empty($id) && is_numeric($id)) {
                $edit_user =  DB::table(TBL_CUSTOMERS)->where('id', '=', $id)->first();
                if ($email == $edit_user->email) {
                    return 'true';
                }
            }
            $users = DB::table(TBL_CUSTOMERS)->where('email', '=', $email)->first();
            if ($users) {
                return 'false';
            } else {
                return 'true';
            }
        } else {
            return 'false';
        }
    }
    public function isActive(Request $request)
    {
        try{
            $customer = Customer::where('id',$request->id)->first();
            if ($customer) {
                if ($customer->is_active== 1) {
                    $customer->is_active = "0";
                } else{
                    $customer->is_active = "1";
                }
                $customer->update();
                return back();
            }   
        }
        catch(Exception $e){
                $Exceptions = New Exceptions;
                $Exceptions->sendException($e);  
        }
    }
    public function getCity(Request $request)
    {
        // return $request->all();
        $cities = City::select('id', 'name')->where('state_id', $request->id)->get();
        $output = [];
        foreach( $cities as $city )
        {
            $output[$city->id] = $city->name;
        }
        return $output;
    }
    public function requestPassword(Request $request)
    {
        try {
            $permission = Permission::permission('request_password');
            if($permission->view==1){
                $data['customers'] = ForgetPassword::select('forget_password.*','customers.*')
                                ->leftjoin('customers','customers.user_id','forget_password.userid')
                                ->get();
                return view('admin.customers.forget-password')->with($data);
            } else{
                return view('layouts.errors.403');
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function requestrestrictionRemove(Request $request)
    {
        try {
            $permission = Permission::permission('restriction_remove');
            if($permission->view==1){
                $data['customers'] = RequestPo::select('request_po.*','customers.*')
                                    ->leftjoin('customers','customers.id','request_po.customer_id')
                                    ->get();
                return view('admin.customers.request_restriction')->with($data);
            } else{
                return view('layouts.errors.403');
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }
    
    
}
