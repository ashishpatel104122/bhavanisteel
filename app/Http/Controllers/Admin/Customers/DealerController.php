<?php

namespace App\Http\Controllers\Admin\Customers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Customers\Customer;
use App\Shop\Dealers\Dealer;
use Carbon\Carbon;
use App\Helpers\Permission;

class DealerController extends Controller
{
    public function index(Request $request){
        try{
            $id = $request->id;
            $data['dealers'] = Dealer::select('dealers.*','customers.name as customer_name')
                            ->leftjoin('customers','customers.id','dealers.parent_id')
                            ->where('dealers.parent_id',$id)
                            ->where('dealers.status',0)
                            ->get();  
            $data['customer'] = Customer::where('id',$id)->first();
            return view('admin.customers.dealers.list')->with($data);
        } catch(Exception $e){
            Log::error($e);
        }
    }
    public function visiteddealer(){
        try{
            $permission = Permission::permission('visited_Dealer');
            if($permission->view==1 || $permission->add==1){
                $data['dealers'] = Dealer::select('dealers.*','customers.name as customer_name')
                                    ->leftjoin('customers','customers.id','dealers.parent_id')
                                    ->where('dealers.deleted_at',null)
                                    ->where('dealers.status',1)
                                    ->get();
                return view('admin.customers.dealers.visitedlist')->with($data);
            } else{
                return view('layouts.errors.403');
            }
        } catch(Exception $e){
            Log::error($e);
        }
    }
    public function destroy($id){
        try{
            $permission = Permission::permission('visited_Dealer');
            if($permission->delete==1){
                $dealer = Dealer::where('id',$id)->first();
                $dealer->deleted_at = date('Y-m-d H:i:s');
                $dealer->save();
                return back()->with('message', 'Dealer delete successfully.');
            } else{
                return view('layouts.errors.403');
            }
        } catch(Exception $e){
            Log::error($e);
        }
    }
}
