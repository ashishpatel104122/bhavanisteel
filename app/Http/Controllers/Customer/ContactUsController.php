<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Cms\ContactUs;
use Validator;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['contactus'] = ContactUs::where('cms_id','!=',0)->first();
            return view('customers.contactus')->with($data);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rules =[
                'name' => 'required|string|max:255',
                'email' => 'required|email|max:255',
                'phone' => 'required|max:15|min:6',
                'message'=>'required',
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
                'name.max' => 'Please enter valid name.',
                'email.required' => 'Please enter email.',
                'email.email' => 'Please enter valid email.',
                'email.max' => 'Please enter valid email.',
                'phone.required' => 'Please enter phone.',
                'phone.max' => 'Please enter valid phone.',
                'phone.min' => 'Please enter valid phone.',
                'message.required' => 'Please enter message.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{  
                $name = trim(strip_tags($request->name));
                $email = trim(strip_tags($request->email));
                $phone = trim(strip_tags($request->phone));
                $message = trim(strip_tags($request->message));
            }
            $contactus=New ContactUs();
            $contactus->name = $name;
            $contactus->email = $email;
            $contactus->phone_no = $phone;
            $contactus->message = $message;
            if ($contactus->save()) {
                return redirect()->route('customer.contactus')->with('message', 'Thank you ContactUs. We will contact you soon.');
            }else{
                return redirect()->route('customer.contactus')->with('error', 'Something went to wrong. Please try again later');
            }
        } catch (Exception $e) {
            Log::error($e);
            
        }
    }
}
