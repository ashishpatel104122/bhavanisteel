<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Cms\ContactUs;

class AboutController extends Controller
{
    public function index()
    {
    	$data['contactus'] = ContactUs::where('cms_id','!=',0)->first();
        return view('customers.aboutus')->with($data);
    }
}
