<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
    	try {
    		$data['is_mobile'] = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    		$data['categorys'] =  Category::where('parent_id',0)->get();
    		$products = Category::select('categories.id as cat_id' , 'categories.name as cat_name','products.*')->leftjoin('products','products.category_id','categories.id');
            if ($request->id) {
                $products->where('categories.id',$request->id);
            }                        

    		$data['products'] = $products->where('products.status',1)->get();
            $data['category_id'] = $request->id;
            $data['category_name'] = Category::where('id',$request->id)->first();
	    	return view('customers/listing')->with($data);
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
    public function getCategoryList(Request $request)
    {
    	try {
            $category_array = array();
			if ($request->id ) {
    			foreach ($request->id as $key => $id) {
    				$category =  Product::select('products.*','categories.id as cat_id' , 'categories.name as cat_name')
    								->leftjoin('categories','categories.id','products.category_id')
    								->where('categories.id',$id)
    								->where('products.status',1)
                                    ->with('product_image');
    				$category_array[] = $category->get();
    			}
			}else{
                $category_array[] =  Product::select('products.*','categories.id as cat_id' , 'categories.name as cat_name')
                                    ->leftjoin('categories','categories.id','products.category_id')
                                    ->where('products.status',1)
                                    ->with('product_image')
    								->get();
			}
			return view('customers.partial.filter',compact('category_array'));
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
    public function getProductModel(Request $request){
        $product_array =  Product::select('products.*')
                                    ->where('products.id',$request->pro_id)
                                    ->where('products.status',1)
                                    ->with('product_image')
                                    ->first();
        return view('customers.partial.modal',compact('product_array'));                            
    }
}
