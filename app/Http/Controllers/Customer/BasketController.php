<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Shop\Products\Product;
use Cookie;
use \stdClass;

class BasketController extends Controller
{
    public function getBasketDetails() {
      $basket = (array) json_decode(Cookie::get('basket'));
      $basketIDs = array_keys($basket);
      $products = Product::findMany($basketIDs);

      $cart = [];
      $subTotal = 0;
      $grandTotal = 0;

      foreach ($products as $prod) {
        $basketItem = $basket[$prod->id];
        $salePrice = (float) $prod->sale_price;
        $total = $salePrice * $basketItem->quantity;

        $cartItem = new stdClass();
        $cartItem->id = $prod->id;
        $cartItem->name = $prod->name;
        $cartItem->sku = $prod->sku;
        $cartItem->description = $prod->description;
        $cartItem->cover = $prod->cover;
        $cartItem->price = $salePrice;
        $cartItem->quantity = $basketItem->quantity;
        $cartItem->total = $total;

        $subTotal += $salePrice;
        $grandTotal += $total;
        $cart[] = $cartItem;
      }

      $data = new stdClass();
      $data->cart = $cart;
      $data->subTotal = $subTotal;
      $data->grandTotal = $grandTotal;

      return $data;
    }
}
