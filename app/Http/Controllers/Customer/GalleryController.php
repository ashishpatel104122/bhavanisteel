<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['categories'] = Category::where('parent_id',0)->get();
            
            $data['products'] = Product::where('status',1)->with('product_image')->get();
            return view('customers.gallery')->with($data);
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
