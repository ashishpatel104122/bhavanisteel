<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Shop\Cms\ContactUs;
use App\Shop\Products\Product;

class HomeController extends Controller
{
    public function index()
    {
        try{
            $data['contactus'] = ContactUs::where('cms_id','!=',0)->first();
            $data['products'] = Product::limit(6)->get();
            return view('customers.home')->with($data);
        }catch(Exception $e){
            Log::error($e);
        }
    }
}
