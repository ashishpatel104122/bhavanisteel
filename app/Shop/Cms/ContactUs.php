<?php

namespace App\Shop\Cms;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
   public $table = "contact_us";
}
