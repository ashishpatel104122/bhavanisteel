<?php

namespace App\Shop\Employees;

use Illuminate\Database\Eloquent\Model;

class AvailableEmployee extends Model
{
    public $table = "available_vendors";
    public $timestamps = false;
}
