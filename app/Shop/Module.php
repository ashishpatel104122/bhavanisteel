<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public $table = "module";
}
