<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $table = "users";
}
