<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;

class Exception extends Model
{
    public $table = "exceptions";
    public $primaryKey = "id";

    public static function insert($e)
    {
    	$exception = New Exception;
    	$exception->exception = $e;
    	$exception->save();

    	// $chk = Mail::send(['html' => 'mail.error.error'],['text' => $e],function($message) {
	    //     $message->to('shineinfosoft27@gmail.com', 'RideApp Developer')->subject
	    //         ('RideApp Error');
	    //     $message->from('rideapp@gmail.com','RideApp');
	    // });
    }
}
