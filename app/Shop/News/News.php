<?php

namespace App\Shop\News;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	public $table = "news";
    public $primaryKey = "id";
    public $timestamps = false;
}
