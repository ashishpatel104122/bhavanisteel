<?php

namespace App\Shop\Products;

use Illuminate\Database\Eloquent\Model;
use App\Shop\Products\ProductImage;

class Product extends Model 
{
    public $table = "products";

 //    public function product_image()
	// {
	// 	return $this->hasMany(ProductImage::class);
	// }
	public function product_image()
    {
        return $this->hasMany('App\Shop\Products\ProductImage','product_id','id');
    }
}
