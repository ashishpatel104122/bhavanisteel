<?php

namespace App\Shop\Products;

use Illuminate\Database\Eloquent\Model;
use App\Shop\Products\Product;

class ProductImage extends Model
{
    public $table = "product_images";

    public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
