<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Shop\Customers\Customer;

class CustomerRestrictionCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customerRestriction:minute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $customers = Customer::where('is_restricted',1)->get();
        foreach ($customers as $customer) {
            date_default_timezone_set('Asia/Kolkata');
            $timeplus = date('Y-m-d H:i:s');
            $restricted_time_plus = date('Y-m-d H:i:s',strtotime('+1 hour',strtotime($customer->restricted_time)));
            if ($restricted_time_plus == $timeplus) {
                $customer->is_restricted = 0;
                $customer->restricted_time = null;
                $customer->save();
            }
        }
        $this->info('Customer restricted check after 1hour');
    }
}
