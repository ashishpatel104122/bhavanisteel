<?php
define('TBL_CUSTOMERS', 'customers');
define('TBL_ORDERS', 'orders');
define('TBL_ORDER_PRODUCT', 'order_product');

define('MAX_IMAGE_SIZE', 2);
define('EMPLOYEE_FILES_PATH', public_path('uploads/customers'));
define('EMPLOYEE_FILES_URL', public_path('uploads/customers'));
